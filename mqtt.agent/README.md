# mqttallosbando

Build and run mqtt.agent

- cd mqtt.agent\mqttagent
- pip install -r requirements.txt
- python manage.py migrate
- python manage.py runserver 127.0.0.1:8000
- python manage.py mqttagent --o start
- python manage.py mqttagent --o discover
