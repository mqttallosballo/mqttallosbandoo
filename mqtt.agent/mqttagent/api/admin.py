from django.contrib import admin
from api.models import *
from simple_history.admin import SimpleHistoryAdmin

from django.contrib.auth.admin import UserAdmin as BaseUserAdmin
from django.contrib.auth.models import User

# Register your models here.

class ProfileInline(admin.StackedInline):
    model = Profile
    can_delete = False
    verbose_name_plural = 'Profiles'

class UserAdmin(BaseUserAdmin):
    inlines = (ProfileInline, )

#admin.site.register(User, SimpleHistoryAdmin)
admin.site.register(Device, SimpleHistoryAdmin)
admin.site.unregister(User)
admin.site.register(User, UserAdmin)
#admin.site.register(Device)
admin.site.register(UserDevice)
admin.site.register(DeviceType)
admin.site.register(CmdType)
admin.site.register(CmdRequest)
admin.site.register(EventType)
admin.site.register(Event)
admin.site.register(Group)