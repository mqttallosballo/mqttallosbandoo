from django.apps import AppConfig
from django.db.models.signals import post_save
from api.signals import create_history

class ApiConfig(AppConfig):
    name = 'api'

    def ready(self):
        from api.models import CmdRequest
        #post_save.connect(create_history, sender=CmdRequest)