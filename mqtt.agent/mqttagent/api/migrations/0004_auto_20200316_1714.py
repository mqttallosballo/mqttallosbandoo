# Generated by Django 3.0.4 on 2020-03-16 16:14

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('api', '0003_auto_20200316_1114'),
    ]

    operations = [
        migrations.AlterField(
            model_name='device',
            name='createdAt',
            field=models.DateTimeField(),
        ),
    ]
