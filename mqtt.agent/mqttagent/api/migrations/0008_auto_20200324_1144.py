# Generated by Django 3.0.4 on 2020-03-24 10:44

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('api', '0007_auto_20200319_1344'),
    ]

    operations = [
        migrations.AddField(
            model_name='cmdrequest',
            name='value',
            field=models.CharField(blank=True, max_length=50),
        ),
        migrations.AlterField(
            model_name='cmdrequest',
            name='deviceId',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.DO_NOTHING, related_name='cmdrequest_related', to='api.Device'),
        ),
        migrations.AlterField(
            model_name='cmdtype',
            name='devType',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.DO_NOTHING, related_name='cmdtype_related', to='api.DeviceType'),
        ),
        migrations.AlterField(
            model_name='device',
            name='macAddress',
            field=models.CharField(max_length=18, unique=True),
        ),
        migrations.CreateModel(
            name='EventType',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('desc', models.CharField(max_length=50)),
                ('devType', models.ForeignKey(null=True, on_delete=django.db.models.deletion.DO_NOTHING, related_name='eventtype_related', to='api.DeviceType')),
            ],
            options={
                'verbose_name': 'CmdType',
                'verbose_name_plural': 'CmdTypes',
            },
        ),
        migrations.CreateModel(
            name='Event',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('status', models.CharField(choices=[('0', 'completed'), ('1', 'error'), ('2', 'unavailable')], default=1, max_length=1)),
                ('result', models.CharField(blank=True, max_length=50)),
                ('createdAt', models.DateTimeField(auto_now=True)),
                ('updatedAt', models.DateTimeField(auto_now=True)),
                ('deviceId', models.ForeignKey(null=True, on_delete=django.db.models.deletion.DO_NOTHING, related_name='event_related', to='api.Device')),
                ('eventType', models.ForeignKey(null=True, on_delete=django.db.models.deletion.DO_NOTHING, related_name='eventType', to='api.EventType')),
            ],
        ),
    ]
