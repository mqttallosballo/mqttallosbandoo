# Generated by Django 3.0.4 on 2020-04-13 15:10

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('api', '0012_auto_20200413_1700'),
    ]

    operations = [
        migrations.AlterField(
            model_name='device',
            name='group',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.DO_NOTHING, related_name='group', to='api.Group'),
        ),
    ]
