from django.db import models

# Create your models here.
from django.contrib.auth.models import User
from simple_history.models import HistoricalRecords
from simple_history import register
from django.conf import settings
from datetime import date
from django.db.models.signals import post_save
from django.dispatch import receiver
import os
import datetime
import json

BROKER_HOST = settings.BROKER['HOST']
BROKER_PORT = settings.BROKER['PORT']
BROKER_PORT_TLS  = settings.BROKER['PORT_TLS']
BROKER_USER  = settings.BROKER['USER']
BROKER_PSW  = settings.BROKER['PASSWORD']
BROKER_CLIENT_ID  = settings.BROKER['CLIENT_ID']


#register user to history
register(User)
# Create your models here.
class Profile(models.Model):
    user = models.OneToOneField(User, related_name='profile', on_delete=models.CASCADE)
    name = models.CharField(max_length=50)
    surname = models.CharField(max_length=50)
    subscribeDate = models.DateField(auto_now=True)

    USER_TYPE = (
        ('0', 'admin'),
        ('1', 'user')
    )

    userType = models.CharField(default=1, max_length=1, choices=USER_TYPE, blank=False, null=False)

    # Un valore "False" vuol dire che il record non è valido. Potrebbe essere stato
    # cancellato oppure annullato!
    isRecordValid = models.BooleanField(blank=False, null=False, default=True)
    isHidden = models.BooleanField(blank=False, null=False, default=False)

    class Meta:
        verbose_name = 'Profilo'
        verbose_name_plural = 'Profili'
    
    def __str__(self):
        return str(self.user.__str__)

class DeviceType(models.Model):
    desc = models.CharField(max_length=50)

    class Meta:
        verbose_name = 'DeviceType'
        verbose_name_plural = 'DeviceTypes'
    
    def __str__(self):
        return str("{} {}".format(self.id, self.desc))       

class UserDevice(models.Model):
    userId = models.ForeignKey(User, related_name='userId', on_delete=models.CASCADE)
    deviceId = models.ForeignKey('Device', related_name='deviceId', on_delete=models.CASCADE)
    dataJoined = models.DateTimeField(auto_now=True)
    ROLES = (
        ('0', 'admin'),
        ('1', 'user')
    )

    role = models.CharField(default=1, max_length=1, choices=ROLES, blank=False, null=False)
    isRecordValid = models.BooleanField(blank=False, null=False, default=True)    

class Device(models.Model):
    deviceType = models.ForeignKey(DeviceType, related_name='deviceType', on_delete=models.DO_NOTHING, null=True, blank=False)
    users = models.ManyToManyField(User, related_name='users', through=UserDevice)
    desc = models.CharField(max_length=50)
    STATE_TYPE = (
        ('0', 'not connected'),
        ('1', 'connected'),
        ('2', 'disconnected'),
        ('3', 'error')
    )

    state = models.CharField(default=1, max_length=1, choices=STATE_TYPE, blank=False, null=False)
    ipAddress = models.CharField(max_length=16)
    macAddress = models.CharField(max_length=18)
    createdAt = models.DateTimeField(auto_now=False, blank=True)
    updatedAt = models.DateTimeField(auto_now=False, blank=True)
    history = HistoricalRecords()
    isRecordValid = models.BooleanField(blank=False, null=False, default=True)

    def save(self, *args, **kwargs):
        if self.pk is None:
            self.createdAt = datetime.datetime.now()
        self.updatedAt = datetime.datetime.now()
        super(Device, self).save(*args, **kwargs)
        # ADD COMMANDS
        defCmd = CmdType.objects.filter(devType=0)
        cmds = CmdType.objects.filter(devType=self.deviceType)
        for cmd in defCmd:
            existCmdReq = CmdRequest.objects.filter(cmdType=cmd, deviceId=self).count()
            if existCmdReq == 0:
                cmdReq = CmdRequest(cmdType=cmd, deviceId=self, status=5)
                cmdReq.save()
        for cmd in cmds:
            existCmdReq = CmdRequest.objects.filter(cmdType=cmd, deviceId=self).count()
            if existCmdReq == 0:
                cmdReq = CmdRequest(cmdType=cmd, deviceId=self, status=5)
                cmdReq.save()      
        # ADD EVENTS
        defEvent = EventType.objects.filter(devType=0)
        events = EventType.objects.filter(devType=self.deviceType)
        for evt in defEvent:
            existEvtReq = Event.objects.filter(eventType=evt, deviceId=self).count()
            if existEvtReq == 0:
                evtReq = Event(eventType=evt, deviceId=self, status=2, result="0")
                evtReq.save()
        for evt in events:
            existEvtReq = Event.objects.filter(eventType=evt, deviceId=self).count()
            if existEvtReq == 0:
                evtReq = Event(eventType=evt, deviceId=self, status=2, result="0")
                evtReq.save()                             

    class Meta:
        verbose_name = 'Device'
        verbose_name_plural = 'Devices'
    
    def __str__(self):
        return str("{} {}".format(self.deviceType, self.desc))       

class DeviceGroup(models.Model):
    device = models.ForeignKey('Device', related_name='device', on_delete=models.CASCADE)
    group = models.ForeignKey('Group', related_name='group', on_delete=models.CASCADE)
    isRecordValid = models.BooleanField(blank=False, null=False, default=True)

class Group(models.Model):
    desc = models.CharField(max_length=50)
    user = models.ForeignKey(User, related_name='user', on_delete=models.DO_NOTHING, null=True, blank=False)
    devices = models.ManyToManyField('Device', related_name='devices', through=DeviceGroup)
    createdAt = models.DateTimeField(auto_now=False, blank=True)
    updatedAt = models.DateTimeField(auto_now=False, blank=True)
    isRecordValid = models.BooleanField(blank=False, null=False, default=True)

    def save(self, *args, **kwargs):
        if self.pk is None:
            self.createdAt = datetime.datetime.now()
        self.updatedAt = datetime.datetime.now()
        super(Group, self).save(*args, **kwargs)

class CmdType(models.Model):
    desc = models.CharField(max_length=50)
    devType = models.ForeignKey(DeviceType, related_name='%(class)s_related', on_delete=models.DO_NOTHING, null=True, blank=False)

    class Meta:
        verbose_name = 'CmdType'
        verbose_name_plural = 'CmdTypes'
    
    def __str__(self):
        return str("{} {}".format(self.devType, self.desc))    

class CmdRequest(models.Model):
    cmdType = models.ForeignKey(CmdType, related_name='cmdType', on_delete=models.DO_NOTHING, null=True, blank=False)
    deviceId = models.ForeignKey(Device, related_name='%(class)s_related', on_delete=models.DO_NOTHING, null=True, blank=False)
    STATUS_TYPE = (
        ('0', 'completed'),
        ('1', 'processing'),
        ('2', 'queue'),
        ('3', 'expired'),
        ('4', 'error'),
        ('5', 'unavailable')
    )
    status = models.CharField(default=1, max_length=1, choices=STATUS_TYPE, blank=False, null=False)
    value = models.CharField(max_length=50, blank=True)
    result = models.CharField(max_length=50, blank=True)
    createdAt = models.DateTimeField(auto_now=True, blank=True)
    updatedAt = models.DateTimeField(auto_now=True, blank=True)

    def save(self, *args, **kwargs):
        if self.status == '2':
            import paho.mqtt.publish as publish
            payload = json.dumps({ 'id' : self.id, 'cmd' : self.cmdType.desc, 'status' : self.status, 'value' : self.value, 'result' : self.result})
            publish.single("devices/{macAddress}/command".format(macAddress=self.deviceId.macAddress), 
                    payload=payload, qos=0, retain=False, hostname=BROKER_HOST,
                    port=BROKER_PORT, client_id=BROKER_CLIENT_ID, keepalive=60, will=None, 
                    auth={"username":BROKER_USER, "password":BROKER_PSW}, tls=None)
            '''
            from mqttclient.MqttClient import MqttClient
            client = MqttClient()
            payload = json.dumps({ 'id' : self.id, 'cmd' : self.cmdType.desc, 'status' : self.status, 'value' : self.value, 'result' : self.result})
            client.publish("devices/{macAddress}/command".format(macAddress=self.deviceId.macAddress),
            payload)
            '''
        super(CmdRequest, self).save(*args, **kwargs)
    
    class Meta:
        verbose_name = 'CmdRequest'
        verbose_name_plural = 'CmdRequests'
    
    def __str__(self):
        return str("{} {}".format(self.cmdType, self.deviceId))    

class EventType(models.Model):
    desc = models.CharField(max_length=50)
    devType = models.ForeignKey(DeviceType, related_name='%(class)s_related', on_delete=models.DO_NOTHING, null=True, blank=False)

    class Meta:
        verbose_name = 'EventType'
        verbose_name_plural = 'EventTypes'
    
    def __str__(self):
        return str("{} {}".format(self.devType, self.desc))   

class Event(models.Model):
    eventType = models.ForeignKey(EventType, related_name='eventType', on_delete=models.DO_NOTHING, null=True, blank=False)
    deviceId = models.ForeignKey(Device, related_name='%(class)s_related', on_delete=models.DO_NOTHING, null=True, blank=False)
    STATUS_TYPE = (
        ('0', 'completed'),
        ('1', 'error'),
        ('2', 'unavailable')
    )
    status = models.CharField(default=1, max_length=1, choices=STATUS_TYPE, blank=False, null=False)
    result = models.CharField(max_length=50, blank=True)
    createdAt = models.DateTimeField(auto_now=True, blank=True)
    updatedAt = models.DateTimeField(auto_now=True, blank=True)

class History(models.Model):
    CMD = 'C'
    EVENT = 'E'
    OTHER = 'O'
    HISTORY_TYPES = (
        (CMD, 'CmdRequest'),
        (EVENT, 'Event'),
        (OTHER, 'Other')
    )
    user = models.ForeignKey(User, on_delete=models.DO_NOTHING)
    historyType = models.CharField(max_length=1, choices=HISTORY_TYPES)
    STATUS_TYPE = (
        ('0', 'completed'),
        ('1', 'processing'),
        ('2', 'queue'),
        ('3', 'expired'),
        ('4', 'error'),
        ('5', 'unavailable')
    )
    status = models.CharField(default=5, max_length=1, choices=STATUS_TYPE, null=True)
    desc = models.CharField(max_length=50, null=True)
    value = models.CharField(max_length=50, null=True)
    result = models.CharField(max_length=50, null=True)
    remoteAddress = models.CharField(max_length=50)
    device = models.ForeignKey(Device, on_delete=models.DO_NOTHING)
    date = models.DateTimeField(auto_now_add=True)