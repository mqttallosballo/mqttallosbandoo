from rest_framework import permissions
from api.models import UserDevice, Device

class IsAdminOrUserJoin(permissions.BasePermission):
    """
    Object-level permission to only allow owners of an object to edit it.
    Assumes the model instance has an `owner` attribute.
    """

    def has_permission(self, request, view):
        # Read permissions are allowed to any request,
        # so we'll always allow GET, HEAD or OPTIONS requests.
        if request.method in 'GET':
            return True
        if request.user.is_superuser:
            return True
        if request.method in 'DELETE':
            usrDev = UserDevice.objects.filter(
                userId=request.user, 
                deviceId__macAddress=view.kwargs['device_macAddress__iexact'],
                pk=view.kwargs['pk'],
                isRecordValid=True).count()
            return usrDev != 0            
        usrDev = UserDevice.objects.filter(
            userId=request.user, 
            deviceId__macAddress=view.kwargs['device_macAddress__iexact'],
            role='0',
            isRecordValid=True).count()
        return usrDev != 0

class IsAdminOrUserDevice(permissions.BasePermission):
    """
    Object-level permission to only allow owners of an object to edit it.
    Assumes the model instance has an `owner` attribute.
    """

    def has_permission(self, request, view):
        # Read permissions are allowed to any request,
        # so we'll always allow GET, HEAD or OPTIONS requests.
        if request.method in 'GET':
            return True
        if request.method in 'POST':
            return True            
        if request.user.is_superuser:
            return True
        if len(view.kwargs)>0:
            if Device.objects.filter(macAddress=view.kwargs['macAddress__iexact'], isRecordValid=True)==0:
                return True
            else:
                usrDev = UserDevice.objects.filter(
                    userId=request.user, 
                    deviceId__macAddress=view.kwargs['macAddress__iexact'],
                    role='0',
                    isRecordValid=True).count()
                return usrDev != 0
        return False