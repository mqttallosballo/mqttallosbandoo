from collections import OrderedDict
from django.contrib.auth.models import User
from rest_framework import serializers
from rest_framework.exceptions import ValidationError
import base64

from api.models import CmdRequest, History

class CmdRequestSerializer(serializers.ModelSerializer):

    class Meta:
        model = CmdRequest
        fields = ('cmdType', 'deviceId', 'status', 'value', 'result', 'createdAt', 'updatedAt')

    def to_representation(self, obj):
        d = OrderedDict()
        try:
            d['id'] = obj.id
            d['deviceId'] = obj.deviceId.id            
            if obj.cmdType is not None:
                y = OrderedDict()
                y['id'] = obj.cmdType.id
                y['desc'] = obj.cmdType.desc
                d['cmdType'] = y
            #d['cmdType'] = obj.cmdType.id            
            if obj.status is not None:
                y = OrderedDict()
                y['id'] = obj.status
                y['desc'] = obj.get_status_display()
                d['status'] = y               
            #d['status'] = obj.status
            d['result'] = obj.result
            d['value'] = obj.value
            d['createdAt'] = obj.createdAt
            d['updatedAt'] = obj.updatedAt
            
        except Exception:
            raise ValidationError("Errore durante l'elaborazione della richiesta")
            # FIXME: Log error!

        return d

    def validate(self, data):
        '''
        Controlla che start sia precedente ad end
        '''
        '''
        if data['start'] > data['end']:
            raise serializers.ValidationError("La data di inizio deve essere precedente alla data di fine")
        return data
        '''
        return data

    def update(self, instance, validated_data):
        request = self.context.get("request")
        CmdRequest.objects.filter(pk=instance.id).update(**validated_data)
        cmd = CmdRequest.objects.get(pk=instance.id)
        x_forwarded_for = request.META.get('HTTP_X_FORWARDED_FOR')
        if x_forwarded_for:
            ip = x_forwarded_for.split(',')[0]
        else:
            ip = request.META.get('REMOTE_ADDR')        
        app = History.objects.create(
            device=cmd.deviceId,
            user=request.user,
            remoteAddress=ip,
            historyType=History.CMD,
            status=cmd.status,
            desc=cmd.cmdType.desc,
            value=cmd.value,
            result=cmd.result
        )
        app.save()
        #post_save.send(CmdRequest, instance=cmd, created=True, user=request.user, remoteAddress=ip)
        cmd.save()
        return cmd

    def delete(self, validated_data):
        '''
        La cancellazione setta il record come non valido, non elimina il record dal db
        '''
        app = Channel.objects.get(validated_data['id'])
        app.is_record_valid = False
        app.save()
        return None
