from collections import OrderedDict
from django.contrib.auth.models import User
from rest_framework import serializers
from rest_framework.exceptions import ValidationError
from api.serializers.UserSerializer import UserSerializer
import base64
import re
from fcm_django.models import FCMDevice

isMacAddress = r"^([0-9a-f]{2}[:-]){5}([0-9a-f]{2})$"

from api.models import Device, UserDevice

class DeviceSerializer(serializers.ModelSerializer):
    class Meta:
        model = Device
        fields = ('deviceType', 'desc', 'state', 'ipAddress', 'macAddress', 'createdAt', 'updatedAt')


    def to_representation(self, obj):
        d = OrderedDict()
        try:
            d['id'] = obj.id
            d['desc'] = obj.desc
            '''
            if obj.group is not None:
                y = OrderedDict()
                y['id'] = obj.group.id
                y['desc'] = obj.group.desc
                d['group'] = y
            '''   
            if obj.deviceType is not None:
                y = OrderedDict()
                y['id'] = obj.deviceType.id
                y['desc'] = obj.deviceType.desc
                d['deviceType'] = y   
            #d['deviceType'] = obj.deviceType.id            
            if obj.state is not None:
                y = OrderedDict()
                y['id'] = obj.state
                y['desc'] = obj.get_state_display()
                d['state'] = y               
            #d['state'] = obj.state
            d['ipAddress'] = obj.ipAddress
            d['macAddress'] = obj.macAddress
            d['createdAt'] = obj.createdAt
            d['updatedAt'] = obj.updatedAt
            
        except Exception:
            raise ParseError(detail="Errore durante l'elaborazione della richiesta")
            # FIXME: Log error!

        return d

    def validate(self, data):
        if self.instance is not None:
            try:
                str(data['macAddress']).lower()
            except Exception:
                data['macAddress'] = str(self.instance.macAddress).lower()
            matches = re.search(isMacAddress, str(data['macAddress']).lower(), re.MULTILINE)
            if matches is None:
                raise ParseError(detail="MAC Address non conforme") 
        return data

    def create(self, data):
        request = self.context.get("request")   
        if(UserDevice.objects.filter(deviceId__macAddress=data['macAddress'], deviceId__isRecordValid=True).count()>0):
            for oldUsrDev in UserDevice.objects.filter(deviceId__macAddress=data['macAddress'], deviceId__isRecordValid=True):
                oldUsrDev.isRecordValid = False
                oldUsrDev.save()        
        if(Device.objects.filter(macAddress=data['macAddress'], isRecordValid=True).count()>0):
            oldDev = Device.objects.get(macAddress=data['macAddress'], isRecordValid=True)
            oldDev.isRecordValid = False
            oldDev.save()            
        fcm_devices = FCMDevice.objects.filter(user=request.user)
        for device in fcm_devices:
            body = "Add device with MAC Address {mac}".format(mac=data['macAddress'])
            device.send_message(
            title="Add Device", body=body, time_to_live=604800,
            click_action="https://psmqtt.ddns.net:8443/devices/{mac}".format(mac=data['macAddress']))
        dev = Device.objects.create(**data)        
        UserDevice.objects.create(userId=request.user, deviceId=dev, role='0')
        return dev

    def delete(self, validated_data):
        return None

'''
class UserDeviceSerializer(serializers.ModelSerializer):
    device = DeviceSerializer()

    class Meta:
        model = UserDevice
        fields = ['device', 'dataJoined', 'role']
        read_only_fields = ('role','dataJoined',)        

    def to_representation(self, obj):
        d = OrderedDict()
        try:
            d['id'] = obj.deviceId.id
            d['desc'] = obj.deviceId.desc
            if obj.deviceId.group is not None:
                y = OrderedDict()
                y['id'] = obj.deviceId.group.id
                y['desc'] = obj.deviceId.group.desc
                d['group'] = y   
            if obj.deviceId.deviceType is not None:
                y = OrderedDict()
                y['id'] = obj.deviceId.deviceType.id
                y['desc'] = obj.deviceId.deviceType.desc
                d['deviceType'] = y   
            #d['deviceType'] = obj.deviceType.id            
            if obj.deviceId.state is not None:
                y = OrderedDict()
                y['id'] = obj.deviceId.state
                y['desc'] = obj.deviceId.get_state_display()
                d['state'] = y               
            #d['state'] = obj.state
            d['ipAddress'] = obj.deviceId.ipAddress
            d['macAddress'] = obj.deviceId.macAddress
            d['createdAt'] = obj.deviceId.createdAt
            d['updatedAt'] = obj.deviceId.updatedAt
            if obj.role != '0':
                d['dataJoined'] = obj.dataJoined
            if obj.role is not None:
                y = OrderedDict()
                y['id'] = obj.role
                y['desc'] = obj.get_role_display()
                d['role'] = y       
            
        except Exception as ex:
            raise ValidationError("Errore durante l'elaborazione della richiesta")
            # FIXME: Log error!

        return d

    def create(self, data):
        raise serializers.ValidationError("Perchè sei tu Romeo?!?")

    def validate(self, data):
        return data

    def update(self, instance, validated_data):
        try:
            Device.objects.filter(macAddress=instance.deviceId.macAddress, isRecordValid=True).update(**validated_data)
            return instance
        except Exception:
            raise serializers.ValidationError("Errore durante l'elaborazione della richiesta")
'''
    