from collections import OrderedDict
from django.contrib.auth.models import User
from rest_framework import serializers
from rest_framework.exceptions import ValidationError
import base64

from fcm_django.models import FCMDevice

from api.models import Event, History, UserDevice

class EventSerializer(serializers.ModelSerializer):

    class Meta:
        model = Event
        fields = ('eventType', 'deviceId', 'status', 'result', 'createdAt', 'updatedAt')

    def to_representation(self, obj):
        d = OrderedDict()
        try:
            d['id'] = obj.id
            d['deviceId'] = obj.deviceId.id            
            if obj.eventType is not None:
                y = OrderedDict()
                y['id'] = obj.eventType.id
                y['desc'] = obj.eventType.desc
                d['eventType'] = y
            #d['cmdType'] = obj.cmdType.id            
            if obj.status is not None:
                y = OrderedDict()
                y['id'] = obj.status
                y['desc'] = obj.get_status_display()
                d['status'] = y               
            #d['status'] = obj.status
            d['result'] = obj.result
            d['createdAt'] = obj.createdAt
            d['updatedAt'] = obj.updatedAt
            
        except Exception:
            raise ValidationError("Errore durante l'elaborazione della richiesta")
            # FIXME: Log error!

        return d

    def validate(self, data):
        return data
    
    def update(self, instance, validated_data):
        request = self.context.get("request")
        Event.objects.filter(pk=instance.id).update(**validated_data)
        evt = Event.objects.get(pk=instance.id)
        x_forwarded_for = request.META.get('HTTP_X_FORWARDED_FOR')
        if x_forwarded_for:
            ip = x_forwarded_for.split(',')[0]
        else:
            ip = request.META.get('REMOTE_ADDR')             
        if(instance.eventType.desc == 'DEV_STATE') or (instance.eventType.desc == 'MANUAL_TRIGGER'):
            if(instance.result != validated_data['result']):
                status = '0' if evt.status == '0' else '5'
                app = History.objects.create(
                    device=evt.deviceId,
                    user=request.user,
                    remoteAddress=ip,
                    historyType=History.EVENT,
                    status=status,
                    desc=evt.eventType.desc,
                    value='',
                    result=evt.result
                )
                app.save()
                usrs = UserDevice.objects.filter(deviceId=instance.deviceId).values('userId')
                fcm_devices = FCMDevice.objects.filter(user__in=usrs)
                if instance.eventType.desc == 'DEV_STATE':
                    body = "{desc}'s state is changed".format(desc=instance.deviceId.desc)
                elif instance.eventType.desc == 'MANUAL_TRIGGER':
                    body = "{desc}'s manual trigger is on".format(desc=instance.deviceId.desc)
                else:
                    body = "Something is changed at device '{desc}'".format(desc=instance.deviceId.desc)
                for device in fcm_devices:
                    #body = "Something is changed at device '{desc}'".format(desc=instance.deviceId.desc)
                    device.send_message(
                    title="Something is changed..", body=body, time_to_live=604800,
                    click_action="https://psmqtt.ddns.net:8443/devices/{mac}".format(mac=instance.deviceId.macAddress))
        return evt


    def delete(self, validated_data):
        app = Channel.objects.get(validated_data['id'])
        app.is_record_valid = False
        app.save()
        return None
