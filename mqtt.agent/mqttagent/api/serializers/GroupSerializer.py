from collections import OrderedDict
from django.contrib.auth.models import User
from rest_framework import serializers
from rest_framework.exceptions import ValidationError, ParseError
import base64
from api.serializers.DeviceSerializer import DeviceSerializer

from api.models import Group, Device, DeviceGroup


class DeviceGroupSerializer(serializers.ModelSerializer):
    class Meta:
        model = Device
        fields = ('desc', 'macAddress')


    def to_representation(self, obj):
        d = OrderedDict()
        try:
            d['desc'] = obj.desc
            d['macAddress'] = obj.macAddress            
        except Exception:
            raise ParseError(detail="Errore durante l'elaborazione della richiesta")
            # FIXME: Log error!

        return d 

    def to_internal_value(self, value):
        try:
            return Device.objects.get(macAddress=value['macAddress'], isRecordValid=True)
        except Exception:
            raise ParseError(detail="Error get Device with macAddress {}".format(value['macAddress']))

class GroupSerializer(serializers.ModelSerializer):
    devices = DeviceGroupSerializer(many=True)
    class Meta:
        model = Group
        fields = ('id', 'desc', 'user', 'createdAt', 'updatedAt', 'devices')


    def validate(self, data):
        return data

    def create(self, data):
        data['user'] = 'A'
        request = self.context.get("request")
        if request and hasattr(request, "user"):
            data['user'] = request.user         
        return Group.objects.create(**data)

    def update(self, instance, validated_data):
        try:
            for dev in validated_data['devices']:
                if DeviceGroup.objects.filter(group=instance, device__macAddress=dev.macAddress).count()>0:
                    devGrp = DeviceGroup.objects.filter(group=instance, device__macAddress=dev.macAddress, isRecordValid=True)
                    for d in devGrp:
                        d.delete()
                instance.devices.add(dev)
        except Exception:
            instance.desc = validated_data.get('desc', instance.desc)
        instance.save()
        return instance

    def delete(self, validated_data):
        app = Group.objects.get(validated_data['id'])
        app.delete()
        return None
