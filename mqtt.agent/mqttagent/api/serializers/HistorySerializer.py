from collections import OrderedDict
from django.contrib.auth.models import User
from rest_framework import serializers
from rest_framework.exceptions import ValidationError
from api.serializers.UserSerializer import UserSerializer
import base64
import re
from fcm_django.models import FCMDevice

isMacAddress = r"^([0-9a-f]{2}[:-]){5}([0-9a-f]{2})$"

from api.models import History

class HistorySerializer(serializers.ModelSerializer):

    class Meta:
        model = History
        fields = ('user', 'historyType', 'remoteAddress', 'date')

    
    def to_representation(self, obj):
        d = OrderedDict()
        try:
            d['id'] = obj.id
            d['username'] = obj.user.username
            if obj.device is not None:
                y = OrderedDict()
                y['macAddress'] = obj.device.macAddress
                y['desc'] = obj.device.desc
                d['device'] = y   
            if obj.historyType is not None:
                y = OrderedDict()
                y['type'] = obj.get_historyType_display()
                if obj.status is not None:
                    y['status'] = obj.get_status_display()
                else:
                    y['status'] = ''
                y['desc'] = obj.desc
                y['value'] = obj.value
                y['result'] = obj.result
                d['detail'] = y               
            #d['state'] = obj.state
            d['remoteAddress'] = obj.remoteAddress
            d['date'] = obj.date
            
        except Exception:
            raise ValidationError("Errore durante l'elaborazione della richiesta")
            # FIXME: Log error!

        return d

    def validate(self, data):
        matches = re.search(isMacAddress, str(data['macAddress']).lower(), re.MULTILINE)
        if matches is None:
            raise ParseError(detail="MAC Address non conforme") 
        return data

    def create(self, data):   
        if(Device.objects.filter(macAddress=data['macAddress'], isRecordValid=True).count()>0):
            oldDev = Device.objects.get(macAddress=data['macAddress'], isRecordValid=True)
            oldDev.isRecordValid = False
            oldDev.save()
        fcm_devices = FCMDevice.objects.filter(user=request.user)
        for device in fcm_devices:
            body = "Edited device with MAC Address {mac}".format(mac=data['macAddress'])
            device.send_message(
            title="Edit Device", body=body, time_to_live=604800,
            click_action="https://psmqtt.ddns.net:8443/devices/{mac}".format(mac=data['macAddress']))
        return Device.objects.create(**data)

    def delete(self, validated_data):
        return None