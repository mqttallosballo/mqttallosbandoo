from collections import OrderedDict
from django.contrib.auth.models import User
from rest_framework import serializers
from rest_framework.exceptions import ValidationError, ParseError
from api.serializers.UserSerializer import UserSerializer
import base64
import re
from fcm_django.models import FCMDevice

isMacAddress = r"^([0-9a-f]{2}[:-]){5}([0-9a-f]{2})$"

from api.models import Device, UserDevice


class JoinSerializer(serializers.ModelSerializer):
    #device = DeviceSerializer()
    #user = UserSerializer()
    username = serializers.CharField(max_length=50)

    class Meta:
        model = UserDevice
        fields = ['dataJoined', 'role', 'userId', 'username']
        read_only_fields = ('dataJoined', 'userId')        

    def to_representation(self, obj):
        d = OrderedDict()
        try:
            d['id'] = obj.id
            d['username'] = obj.userId.username
            d['dataJoined'] = obj.dataJoined
            if obj.role is not None:
                y = OrderedDict()
                y['id'] = obj.role
                y['desc'] = obj.get_role_display()
                d['role'] = y       
            
        except Exception as ex:
            raise ParseError(detail="Errore durante l'elaborazione della richiesta")
            # FIXME: Log error!

        return d

    
    def create(self, data):
        macAddress = self.context.get('view').kwargs.get('device_macAddress__iexact')
        dev = Device.objects.filter(macAddress=macAddress, isRecordValid=True)
        usr = User.objects.filter(username=data['username'])
        if dev is not None and usr is not None:
            if UserDevice.objects.filter(userId__in=usr, deviceId__in=dev, isRecordValid=True).count()>0:
                raise ParseError(detail="Utente già associato")
            return UserDevice.objects.create(
                userId = usr[0],
                deviceId = dev[0],
                role = data['role'])
        else:
            raise ParseError(detail="Username e/o MAC Address non corretti o non disponibili")
    
    def delete(self, validated_data):
        app = UserDevice.objects.get(validated_data['id'])
        app.isRecordValid = True
        app.save()
        return None