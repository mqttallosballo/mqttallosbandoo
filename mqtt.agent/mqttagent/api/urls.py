from django.conf.urls import url, include
# from rest_framework import routers
from rest_framework_nested import routers
from api.views.UserViewSet import UserViewSet
from api.views.DeviceViewSet import DeviceViewSet
from api.views.HistoryViewSet import HistoryViewSet
from api.views.JoinViewSet import JoinViewSet
from api.views.CmdRequestViewSet import CmdRequestViewSet
from api.views.EventViewSet import EventViewSet
from api.views.GroupViewSet import GroupViewSet


router = routers.SimpleRouter(trailing_slash=False)
router.register(r'user/', UserViewSet, basename='user')
router.register(r'device/?(?P<group>[0-9]{10})$', DeviceViewSet, basename='device-group')
router.register(r'device', DeviceViewSet, basename='device')
#router.register(r'history', HistoryViewSet, basename='history')
router.register(r'group', GroupViewSet, basename='group')

device_router = routers.NestedSimpleRouter(router, r'device', lookup='device')
device_router.register(r'command', CmdRequestViewSet, basename='device-command')
device_router.register(r'event', EventViewSet, basename='device-event')
device_router.register(r'history', HistoryViewSet, basename='device-history')
device_router.register(r'join', JoinViewSet, basename='device-join')

#router.register(r'info', CallInfoViewSet, base_name='call')

urlpatterns = [
    url(r'^', include(router.urls)),
    url(r'^', include(device_router.urls)),
]