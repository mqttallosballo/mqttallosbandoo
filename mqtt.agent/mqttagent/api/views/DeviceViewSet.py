from django.contrib.auth.models import User
from django.http import HttpResponse
from rest_framework import status, viewsets, mixins
from rest_framework.response import Response
from rest_framework.permissions import IsAuthenticated, DjangoModelPermissions
from api.permissions.BasePermission import IsAdminOrUserDevice
from rest_framework.decorators import action
import os
from api.serializers.DeviceSerializer import DeviceSerializer
from api.serializers.CmdRequestSerializer import CmdRequestSerializer
from api.models import Device, CmdRequest, UserDevice


class DeviceViewSet(viewsets.ModelViewSet):
    queryset = Device.objects.none()
    serializer_class = DeviceSerializer
    permission_classes = (IsAuthenticated, IsAdminOrUserDevice)
    lookup_field = 'macAddress__iexact'

    '''
    def get_serializer_class(self):
        if len(self.kwargs) == 0:
            if self.action == 'create':
                return DeviceSerializer
            if self.action == 'partial_update':
                return DeviceSerializer
        return UserDeviceSerializer
    '''
    def get_queryset(self):
        query_params = self.request.query_params
        group = query_params.get('group', None)
        filter_args = {}

        #if group is not None:
        #    filter_args['group'] = group
        user = self.request.user
        filter_args['isRecordValid'] = True
        
        if user.is_superuser or user.username == 'agent':
            return Device.objects.filter(**filter_args)
        else:
            return Device.objects.filter(users=user, **filter_args)

    def destroy(self, request, pk=None):
        instance = self.get_object()
        # instance.isActive = False
        instance.isRecordValid = False
        instance.save()
        return Response(status=status.HTTP_204_NO_CONTENT)

