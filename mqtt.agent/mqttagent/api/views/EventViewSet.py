from django.contrib.auth.models import User
from django.http import HttpResponse
from rest_framework import status, viewsets, mixins
from rest_framework.response import Response
from rest_framework.permissions import IsAuthenticated, DjangoModelPermissions
from rest_framework.decorators import action
import os
from api.serializers.DeviceSerializer import DeviceSerializer
from api.serializers.EventSerializer import EventSerializer
from api.models import Device, Event


class EventViewSet(viewsets.ModelViewSet):
    """
    Lista chiamate, accetta GET/DELETE.
    Dettaglio chiamate, accetta anche POST/PUT/PATCH.
    """
    queryset = Event.objects.none()
    serializer_class = EventSerializer
    permission_classes = (IsAuthenticated, )
    lookup_field = 'eventType__desc__iexact'

    def get_queryset(self):
        query_params = self.request.query_params
        # number_call = query_params.get('number_call', None)
        # company_id = query_params.get('company_id', None)
        # subthings = query_params.get('subthing', None)
        # years = query_params.get('year', None)
        user = self.request.user
        return Event.objects.filter(deviceId__macAddress=self.kwargs['device_macAddress__iexact'], deviceId__isRecordValid=True)
        '''
        if user.is_superuser:
            return CmdRequest.objects.filter()
        # L'utente puo essere uno studente o meno, vedra solo i propri dati.
        else:
            return CmdRequest.objects.filter(deviceId__macAddress=self.kwargs['device_macAddress__iexact'])
        '''

    def destroy(self, request, pk=None):
        """
        Previene la cancellazione del record dal db, che manteniamo per avere uno storico.
        """
        instance = self.get_object()
        # instance.isActive = False
        instance.isRecordValid = False
        instance.save()
        return Response(status=status.HTTP_204_NO_CONTENT)
