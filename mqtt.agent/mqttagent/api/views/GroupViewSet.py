from django.contrib.auth.models import User
from django.http import HttpResponse
from rest_framework import status, viewsets, mixins
from rest_framework.response import Response
from rest_framework.permissions import IsAuthenticated, DjangoModelPermissions
from rest_framework.decorators import action
import os
from api.serializers.GroupSerializer import GroupSerializer
from api.models import Group


class GroupViewSet(viewsets.ModelViewSet):
    """
    Lista chiamate, accetta GET/DELETE.
    Dettaglio chiamate, accetta anche POST/PUT/PATCH.
    """
    queryset = Group.objects.none()
    serializer_class = GroupSerializer
    permission_classes = (IsAuthenticated, )
    #lookup_field = 'macAddress__iexact'

    def get_queryset(self):
        query_params = self.request.query_params
        # number_call = query_params.get('number_call', None)
        # company_id = query_params.get('company_id', None)
        # subthings = query_params.get('subthing', None)
        # years = query_params.get('year', None)
        user = self.request.user
        if user.is_superuser:
            return Group.objects.filter(isRecordValid=True)
        # L'utente puo essere uno studente o meno, vedra solo i propri dati.
        else:
            return Group.objects.filter(user=user, isRecordValid=True)

    def destroy(self, request, pk=None):
        """
        Previene la cancellazione del record dal db, che manteniamo per avere uno storico.
        """
        instance = self.get_object()
        # instance.isActive = False
        instance.isRecordValid = False
        instance.save()
        return Response(status=status.HTTP_204_NO_CONTENT)
