from django.contrib.auth.models import User
from django.http import HttpResponse
from django.db.models import Q
from rest_framework import status, viewsets, mixins
from rest_framework.response import Response
from rest_framework.permissions import IsAuthenticated, DjangoModelPermissions
from django.contrib.contenttypes.models import ContentType
from rest_framework.decorators import action
import os
from api.serializers.DeviceSerializer import DeviceSerializer
from api.serializers.HistorySerializer import HistorySerializer
from api.models import History, CmdRequest, UserDevice


class HistoryViewSet(viewsets.ModelViewSet):
    queryset = History.objects.none()
    serializer_class = HistorySerializer
    permission_classes = (IsAuthenticated, )
    #lookup_field = 'device__macAddress__iexact'

    def get_queryset(self):
        query_params = self.request.query_params
        datefrom = query_params.get('datefrom', None)
        dateto = query_params.get('dateto', None)
        filter_args = {}        
        if datefrom is not None:
            filter_args['date__gte'] = datefrom
        
        if dateto is not None:
            filter_args['date__lte'] = dateto
        
        #GET ROLE TO USER REQUEST
        role = UserDevice.objects.filter(
            deviceId__macAddress=self.kwargs['device_macAddress__iexact'], 
            userId=self.request.user,
            deviceId__isRecordValid=True,
            isRecordValid=True,
            role='0'
            ).values('role')
        filter_args['device__isRecordValid'] = True
        #filter_args['device__pk__in'] = usrDev
        filter_args['device__macAddress'] = self.kwargs['device_macAddress__iexact']
        
        if len(role) > 0:
            return History.objects.filter(**filter_args).order_by('-date')
        else:
            return History.objects.filter(Q(user=self.request.user, **filter_args) | Q(user__username='agent', **filter_args)).order_by('-date')
        

    def destroy(self, request, pk=None):
        instance = self.get_object()
        # instance.isActive = False
        instance.isRecordValid = False
        instance.save()
        return Response(status=status.HTTP_204_NO_CONTENT)
