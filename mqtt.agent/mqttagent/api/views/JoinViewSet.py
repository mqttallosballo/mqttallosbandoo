from django.contrib.auth.models import User
from django.http import HttpResponse
from django.db.models import Q
from rest_framework import status, viewsets, mixins
from rest_framework.response import Response
from rest_framework.permissions import IsAuthenticated, DjangoModelPermissions
from api.permissions.BasePermission import IsAdminOrUserJoin
from django.contrib.contenttypes.models import ContentType
from rest_framework.decorators import action
import os
from api.serializers.JoinSerializer import JoinSerializer
from api.models import History, CmdRequest, UserDevice


class JoinViewSet(mixins.CreateModelMixin,
                                mixins.ListModelMixin,
                                viewsets.GenericViewSet):
    queryset = UserDevice.objects.none()
    serializer_class = JoinSerializer
    permission_classes = (IsAuthenticated, IsAdminOrUserJoin)
    #lookup_field = 'device__macAddress__iexact'

    def get_queryset(self):
        query_params = self.request.query_params
        group = query_params.get('group', None)
        
        filter_args = {}

        #GET ROLE TO USER REQUEST
        role = UserDevice.objects.filter(
            deviceId__macAddress=self.kwargs['device_macAddress__iexact'], 
            userId=self.request.user,
            deviceId__isRecordValid=True,
            isRecordValid=True,
            role='0'
            ).values('role')
        filter_args['deviceId__macAddress'] = self.kwargs['device_macAddress__iexact']
        filter_args['deviceId__isRecordValid'] = True
        filter_args['isRecordValid'] = True
        if len(role)>0:        
            return UserDevice.objects.filter(**filter_args).order_by('role')
        else:
            return UserDevice.objects.filter(Q(userId=self.request.user, **filter_args) | Q(userId__username='agent', **filter_args)).order_by('role')
    
    def destroy(self, request, *args, **kwargs):
        app = UserDevice.objects.get(id=kwargs.get('pk'))
        app.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)
        
