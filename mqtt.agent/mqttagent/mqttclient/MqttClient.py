import paho.mqtt.client as mqtt #import the client1
from django.conf import settings
import uuid

BROKER_HOST = settings.BROKER['HOST']
BROKER_PORT = settings.BROKER['PORT']
BROKER_PORT_TLS  = settings.BROKER['PORT_TLS']
BROKER_USER  = settings.BROKER['USER']
BROKER_PSW  = settings.BROKER['PASSWORD']
BROKER_CLIENT_ID  = settings.BROKER['CLIENT_ID']

client = mqtt.Client(BROKER_CLIENT_ID+"-"+str(uuid.uuid4()))
client.username_pw_set(username=BROKER_USER, password=BROKER_PSW)
client.connect(host=BROKER_HOST, port=BROKER_PORT)

stateCallback = None
resultCallback = None

class MqttClient:
    username = ''
    password = ''
    brokerAddress = ''
    port = ''
    mqtt = None


    def __init__(self):
        self.mqtt = client
        self.mqtt.on_message = self.onMessage
    
    def disconnect(self):
        self.mqtt.disconnect()

    def subscribe(self, topic, qos=0, options=None, properties=None):
        self.mqtt.subscribe(topic)
        self.mqtt.loop_start()

    def onMessageCallback(self, callback, topic):
        if topic.split("/")[2] == 'state':
            global stateCallback
            stateCallback = callback
        elif topic.split("/")[2] == 'result':
            global resultCallback
            resultCallback = callback
        #self.mqtt.on_message = self.onMessage

    def publish(self, topic, payload):
        msg = self.mqtt.publish(topic, payload=payload, qos=0)

    @staticmethod
    def onMessage(client, userdata, message):
        if message.topic.split("/")[2] == 'state':
            global stateCallback
            stateCallback(client, userdata, message)
        elif message.topic.split("/")[2] == 'result':
            global resultCallback
            resultCallback(client, userdata, message)