import aiohttp
import asyncio
import requests
import time
from api.models import Device
from django.conf import settings
import datetime
import logging
logger = logging.getLogger('mqttagentDiscover')

URL = settings.AGENT['URL']
API_VERSION = settings.AGENT['API_VERSION']
API_DEVICE = '/device'
API_DEVICE_DETAIL = '/device/{id}'
API_COMMAND = '/device/{id}/command'
API_COMMAND_DETAIL = '/device/{id}/command/{cmd}'
API_EVENT = '/device/{id}/event'
API_EVENT_DETAIL = '/device/{id}/event/{evt}'


async def check(device, session, **kwargs) -> str:
    now = datetime.datetime.utcnow()
    devDate = datetime.datetime.strptime(device['updatedAt'].split(".",1)[0], '%Y-%m-%dT%H:%M:%S')
    if(((now - devDate).seconds / 60) > 1 and device['state']['id'] != '0'):
        logger.info(" {} not connected".format(device['macAddress']))
        payload = { "state" : 0 }
        resp = await session.request(method="PATCH", url=URL+API_VERSION+API_DEVICE_DETAIL.format(id=device['macAddress']), data=payload)
        resp.raise_for_status()
        payload = { "result" : 0 }
        resp = await session.request(method="PATCH", url=URL+API_VERSION+API_EVENT_DETAIL.format(id=device['macAddress'], evt="DEV_STATE"), data=payload)
        resp.raise_for_status()        
        logger.info("Got response {} for URL: {}".format(resp.status, device['macAddress']))
        resp = await session.request(method="GET", url=URL+API_VERSION+API_COMMAND.format(id=device['macAddress']))
        resp.raise_for_status()        
        logger.info("Got response {} for URL: {}".format(resp.status, device['macAddress']))
        cmds = await resp.json()
        for cmd in cmds:
            if cmd['status']['id'] == "2":
                payload = { "status" : 3 }
                resp = await session.request(method="PATCH", url=URL+API_VERSION+API_COMMAND_DETAIL.format(id=device['macAddress'], cmd=cmd['cmdType']['desc']), data=payload)
                resp.raise_for_status()        
                logger.info("Got response {} for URL: {}".format(resp.status, device['macAddress']))

        #check sui comandi

        # json = await resp.json()
    else:
        logger.info(" {} is connected or not connected.. for now".format(device['macAddress']))
    return 0

async def discover(devices, **kwargs) -> None:
    headers={'Authorization':settings.AGENT['TOKEN']}
    async with aiohttp.ClientSession(headers=headers, connector=aiohttp.TCPConnector(verify_ssl=False)) as session:
        tasks = []
        for device in devices:
            tasks.append(
                check(device=device, session=session, **kwargs)
            )
        await asyncio.gather(*tasks)

def main():
    while True:
        headers={'Authorization':settings.AGENT['TOKEN']}
        r = requests.get(URL+API_VERSION+API_DEVICE, verify=False, headers=headers)
        devices = r.json()
        logger.info("Asyncio start")
        loop = asyncio.get_event_loop()
        loop.run_until_complete(discover(devices))
        logger.info("Asyncio finish")
        time.sleep(30)