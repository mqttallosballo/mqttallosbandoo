from threading import Thread
from django.conf import settings
import json
import requests
import logging
logger = logging.getLogger('cmdhandler')

URL = settings.AGENT['URL']
API_VERSION = settings.AGENT['API_VERSION']
API_DEVICE = '/device'
API_DEVICE_DETAIL = '/device/{id}'
API_COMMAND = '/device/{id}/command'
API_COMMAND_DETAIL = '/device/{id}/command/{cmd}'


class CmdResultHandler(Thread):
    topic = ''
    qos = 0
    url = ''
    mqttClient = None

    def __init__(self, mqttClient, topic, url):
        Thread.__init__(self)
        self.topic = topic
        self.url = url
        self.mqttClient = mqttClient
        self.mqttClient.onMessageCallback(callback=self.onMessage, topic=self.topic)

    def run(self):
        logger.info("start thread on topic {}".format(self.topic))
    
    @staticmethod
    def onMessage(client, userdata, message):
        try:
            cmd = json.loads(message.payload.decode('utf-8'))
            macAddress = message.topic.split("/")[1]
            #logger.info("New cmd result from {} = {}".format(macAddress, message.payload))
            logger.info("New cmd result from {}".format(macAddress))
            data = json.loads('{ "result" : "'+str(cmd['result'])+'", "status" : "'+str(cmd['status'])+'", "value" : "'+str(cmd['value'])+'"}')
            headers = {
                'Authorization':settings.AGENT['TOKEN']
            }            
            r = requests.patch(URL+API_VERSION+API_COMMAND_DETAIL.format(id=macAddress, cmd=cmd['cmd']), verify=False, headers=headers, data=data)
            #logger.info(r.status_code)
            #logger.info(r.text)
        except Exception as ex:
            logger.error(str(ex))        
    