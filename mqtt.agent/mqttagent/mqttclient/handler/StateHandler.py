from threading import Thread
from django.conf import settings
import json
import requests
import logging
logger = logging.getLogger('statehandler')

URL = settings.AGENT['URL']
API_VERSION = settings.AGENT['API_VERSION']
API_DEVICE = '/device'
API_DEVICE_DETAIL = '/device/{id}'
API_COMMAND = '/device/{id}/command'
API_COMMAND_DETAIL = '/device/{id}/command/{cmd}'
API_EVENT = '/device/{id}/event'
API_EVENT_DETAIL = '/device/{id}/event/{evt}'

class StateHandler(Thread):
    topic = ''
    qos = 0
    url = ''
    mqttClient = None

    def __init__(self, mqttClient, topic, url):
        Thread.__init__(self)
        self.topic = topic
        self.url = url
        self.mqttClient = mqttClient
        self.mqttClient.onMessageCallback(callback=self.onMessage, topic=self.topic)

    def run(self):
        logger.info("start thread on topic {}".format(self.topic))
    
    @staticmethod
    def onMessage(client, userdata, message):
        try:
            state = json.loads(message.payload.decode('utf-8'))
            macAddress = message.topic.split("/")[1]
            #logger.info("New state from {} = {}".format(macAddress, message.payload))
            logger.info("New state from {}".format(macAddress))
            headers = {
                'Authorization':settings.AGENT['TOKEN']
            }
            for key, value in state.items():
                if str(key).lower() == 'state':
                    r = requests.patch(URL+API_VERSION+API_DEVICE_DETAIL.format(id=macAddress), verify=False, headers=headers, data=state)
                    #logger.info(r.status_code)
                    #logger.info(r.text)
                elif str(key).lower() == 'ipAddress':
                    pass
                else:
                    data = json.loads('{ "result" : "'+str(value)+'", "status" : 0 }')
                    r = requests.patch(URL+API_VERSION+API_EVENT_DETAIL.format(id=macAddress, evt=key), verify=False, headers=headers, data=data)
                    #logger.info(r.status_code)
                    #logger.info(r.text)                    
        except Exception as ex:
            logger.error(str(ex))
    