import sys
from django.conf import settings
from django.core.management.base import BaseCommand, CommandError
from mqttclient.handler.StateHandler import StateHandler
from mqttclient.handler.CmdResultHandler import CmdResultHandler
from mqttclient.MqttClient import MqttClient
import mqttclient.discover.DeviceDiscover as Discover

import logging
logger = logging.getLogger('mqttagent')


URL = settings.AGENT['URL']
API_VERSION = settings.AGENT['API_VERSION']
API_DEVICE = '/device'
API_DEVICE_DETAIL = '/device/{id}'
API_COMMAND = '/device/{id}/command'
API_COMMAND_DETAIL = '/device/{id}/command/{cmd}'


class Command(BaseCommand):
    help = 'Mqtt Agent'

    def add_arguments(self, parser):
        parser.add_argument(
            '--o',
            type=str,
            help='Operation like "start, stop and restart"',
        )                          

    def handle(self, *args, **options):
        logger.info("Start mqttagent")
        if len(options) > 0:
            op = options['o'] 
            if op == 'start':
                logger.info("Run State and Cmd Handler")
                client = MqttClient()
                client.subscribe([("devices/+/state", 0), ("devices/+/result", 0)])
                th = StateHandler(client, "devices/+/state", URL+API_DEVICE_DETAIL)
                ty = CmdResultHandler(client, "devices/+/result", URL+API_DEVICE_DETAIL)
                th.start()
                ty.start()
                while True:
                    pass
                pass
            elif op == 'restart':
                pass
            elif op == 'stop':
                pass
            elif op == 'discover':
                logger.info("Start mqttagent device discover")
                Discover.main()
        else:
            print("quit")