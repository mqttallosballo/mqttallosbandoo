How setup your mosquitto broker:
*  Following [link](https://mosquitto.org/download/) to install mosquitto
*  Create your user file and add "admin": mosquitto_passwd -c pwfile admin
*  Add "device" to user file: mosquitto_passwd pwfile device
*  Copy files aclfile and mosquitto.conf
*  Edit mosquitto.conf typing your own ip address and aclfile path and pwfile path
*  Run mosquitto: mosquitto -c mosquitto.conf