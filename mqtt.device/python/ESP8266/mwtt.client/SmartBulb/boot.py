
import os
import time
import uasyncio as asyncio
from umqttsimple import MQTTClient
import machine
from machine import Pin
import network
import esp
esp.osdebug(None)
import gc


def reset_file():
    if 'wifi.txt' in os.listdir():
        os.remove('wifi.txt')
    with open('firstBoot', 'w+') as f:
        f.close()


def chekc_wifi_credentials(file):
    #ssid = password = mqttusername = mqttpassword = mqttdevicename = " "
    print('Proverò a legger il file : %s' % (file))
    try:
        temp=''
        with open(file, "r") as f:
            temp = f.read()
            f.close()
        print('wifi cred:' + temp)
        #ssid, password, mqttusername, mqttpassword, mqttdevicename = temp.split('|')
        # password = temp.split('|')[1]
        if 'AP_mode' in os.listdir():
            AP_mode = True
            os.remove(file)
        else:
            AP_mode = False
    except:
        AP_mode = True
    #return ssid, password, mqttusername, mqttpassword, mqttdevicename, AP_mode
    if len(temp) > 0:
        return temp.split('|')[0], temp.split('|')[1], temp.split('|')[2] , temp.split('|')[3], temp.split('|')[4], AP_mode
    else:
        return "", "", "", "", "", AP_mode

async def wifi_connction(ssid,password):
    global station
    station.active(True)
    station.connect(ssid, password)
    print("ho passato la connect")
    #isConnected=False
    while station.isconnected() == False:
        await asyncio.sleep(0.2)
        #print("sto nel maledeto while isconnect")
    global ipAddress
    ipAddress = station.ifconfig()[0]
    #isConnected = True
    print(ipAddress, station.ifconfig()[1], station.ifconfig()[2], station.ifconfig()[3])


async def led_blinking():
    pass
#    print('Blinko asincrono! fuck yeah')
#    while blinking:
#        #led.value(0)
#        print('18 \n')
#        print('1 \n')
#        await asyncio.sleep(0.7)

async def check_is_connected(ssid, password):
    try:
        await asyncio.wait_for(wifi_connction(ssid, password),300)
    except asyncio.TimeoutError:
        if 'firstBoot' in os.listdir():
            with open('AP_mode', 'w') as f:
                f.write('Connessione WiFi non possibbile. Controllare SSID e password.')
                f.close()
        machine.reset()
    #asyncio.cancel(led_blinking())

def loop_():
    loop = asyncio.get_event_loop()
    #loop.create_task(led_blinking())
    loop.run_until_complete(check_is_connected(ssid, password))
    loop.close()
    del loop
#    global blinking
#    blinking = False
#    try:
#        asyncio.cancel(led_blinking())
#    except asyncio.CancelledError:
#        return

def button_reset():
    resetButton = Pin(0, Pin.IN, Pin.PULL_UP)
    finished=0
    pushed=0
    while finished <= 14:
        print('finished: %s and pushed: %s' % (finished, pushed))
        pushed += resetButton.value()
        time.sleep(0.25)
        finished += 1
    if int(pushed) < 8:
        reset_file()
        finished = 0
        while finished < 1000:
            print('antonio|pietro^2')
            finished += 1
        machine.reset()
    del finished,pushed,resetButton


station = network.WLAN(network.STA_IF)
station.active(False)
ap_if = network.WLAN(network.AP_IF)
ap_if.active(False)

button_reset()

#blinking = True

password = " "
ssid = " "
mqttusername = " "
mqttpassword = " "
mqttdevicename = " "
ipAddress = " "
#time.sleep(0.500)
mqtt_server = 'YOUR_SERVER'


try:
    client_id = ''
    for ot in list(network.WLAN().config('mac')): h = hex(ot); client_id += h[2] + h[3] + ':'
except:
    client_id="00:00:00:00:00:11:"

print('mac : %s' % mac)

client_id = mac #ubinascii.hexlify(machine.unique_id())
topic_sub_result = b'devices/' + mac + '/result'
topic_sub = b'devices/' + mac + '/command'
topic_pub = b'devices/' + mac + '/state'


ssid, password, mqttusername, mqttpassword, mqttdevicename, AP_mode = chekc_wifi_credentials('wifi.txt')
print('imposto AP? NON trovato wifi.txt? : %s' % AP_mode)
showHtml=False

if not AP_mode:
#    print('ho trovato il file con le credenziali del wifi')
    loop_()
else:
#    print("imposto il mio AP")
    ap_if.active(True)
    ap_if.config(essid="MQTTallosbando",password="11111111")
    showHtml=True
print('fine boot')
gc.collect()

