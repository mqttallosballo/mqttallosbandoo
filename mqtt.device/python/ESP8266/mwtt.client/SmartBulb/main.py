
import queues


def sub_cb(topic, msg):
    print((topic, msg))
    #global queueHandler
    queueHandler.put_nowait(msg)
    #testo = queueHandler.get_nowait()
    #print(testo)
    # if topic == b'notification' and msg == b'received':
    # print('ESP received hello message')


def set_value(jsonObj):
    print("set value!")
    import ujson
    global topic_sub_result
    pinIO.value(int(jsonObj["value"]))
    jsonObj["status"] = 0
    publish_message(client, topic_sub_result, ujson.dumps(jsonObj))

def get_value(jsonObj):
    import ujson
    print("get value!")
    global topic_sub_result
    jsonObj["result"] = pinIO.value()
    jsonObj["status"] = 0
    publish_message(client, topic_sub_result, ujson.dumps(jsonObj))

def set_state(jsonObj):
    print("set state!")
    pass

def get_state(jsonObj):
    print("get state!")
    pass

async def queue_handler():
    import ujson
    while True:
        await asyncio.sleep(1)
        #print("queue handler is alive bitch!")
        if not queueHandler.empty():
            jsonObj = ujson.loads(queueHandler.get_nowait().decode('utf8').replace("'", '"'))
            fun = jsonObj["cmd"]
            if fun == "SET_VALUE":
                set_value(jsonObj)
            if fun == "GET_VALUE":
                get_value(jsonObj)
            if fun == "GET_STATE":
                get_state(jsonObj)
            if fun == "SET_STATE":
                set_state(jsonObj)




def connect_and_subscribe():
    #print("Inizio la richiesta!")
    # client = MQTTClient(client_id = client_id, server = mqtt_server, port = 58553, user = "device", password = "device", keepalive = 0, ssl=True, ssl_params={"server_side":False, "cert_reqs":ussl.CERT_NONE, "cert":cert_data, "key":key_data})
    client = MQTTClient(client_id=client_id, server=mqtt_server, port=1553, user="device", password="device",
                        keepalive=0, ssl=False)
    client.set_callback(sub_cb)
    print("Set callback  fatta!")
    client.connect()
    print("Connessione fatta!")
    client.subscribe(topic_sub)
    #print('Connected to %s MQTT broker, subscribed to %s topic' % (mqtt_server, topic_sub))
    return client

def restart_and_reconnect():
    print('Failed to connect to MQTT broker. Reconnecting...')
    time.sleep(10)
    machine.reset()



def html_decode(s):
    """
    Returns the ASCII decoded version of the given HTML string. This does
    NOT remove normal HTML tags like <p>.
    """
    htmlCodes = (
        ("'", '&#39;'),
        ('%', '%25'),
        ('"', '&quot;'),
        ('>', '&gt;'),
        ('<', '&lt;'),
        ('&', '&amp;')
    )
    for code in htmlCodes:
        s = s.replace(code[1], code[0])
    return s



async def send_status(status):
    while True:
        try:
            await asyncio.sleep(5)
            client.check_msg()
            msg = '{"state": ' + str(status) + ' , "DEV_VALUE":' + str(pinIO.value()) + ', "DEV_STATE":' + str(1) + ', "MANUAL_TRIGGER":' + str(0) + '}'
            publish_message(client,topic_pub, msg)

        except OSError as e:
            print("Except osrerror!")
            print(e)

def publish_message(client,topic_pub,msg):
    try:
        print("pubblish : %s" % (topic_pub))
        print("msg : %s" % (msg))
        client.publish(topic_pub, msg)
    except OSError as e:
        print("Except osrerror!")
        print(e)

def html_page():
    try:
        import usocket as socket
    except:
        import socket
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    s.bind(('', 80))
    s.listen(5)
    while showHtml:
        import webpage
        conn, addr = s.accept()
        print('Got a connection from %s' % str(addr))
        request = str(conn.recv(1024))
        print('Content = %s' % request)
        # print('Content trunked = %s' % request[:100])
        # print(request[:100].find('SSID='))
        #response = web_page()
        if (request[:150].find('SSID=') > -1) and (request[:150].find('password=') > -1) and (
                request[:150].find('MQTTUSERNAME=') > -1) and (request[:150].find('MQTTPASSWORD=') > -1) and (
                request[:150].find('MQTTDEVICENAME=') > -1):
            import ure
            match = ure.search(
                "SSID=([^&]*)&password=([^&]*)&MQTTUSERNAME=([^&]*)&MQTTPASSWORD=([^&]*)&MQTTDEVICENAME=([^&]*)",
                request[:150])
            print(
                'crendetials to be written = {} | {} | {} | {} | {}'.format(html_decode(match.group(1)) , html_decode(str(match.group(2)).split()[0]),  html_decode(str(match.group(3)).split()[0]),  html_decode(str(match.group(4)).split()[0]),
                                                                            html_decode(str(match.group(5)).split()[0])))
            with open('wifi.txt', 'w') as f:
                f.write(html_decode(match.group(1)) + "|" + html_decode(str(match.group(2)).split()[0]) + "|" + html_decode(str(match.group(3)).split()[0]) + "|" + html_decode(str(match.group(4)).split()[0]) + "|" + html_decode(str(match.group(5)).split()[0]))
                f.close()
            #response = web_page()
            conn.write("<h1>RESETTING!!!</h1>")
            conn.close()
            machine.reset()
            #
        else:
            del request
            #conn.write("{'Content-Type': 'text/html; encoding=utf8', 'Content-Length':" + str(len(web_page())) + ", 'Connection': 'close',)} \\n")
            conn.write(webpage.web_page(ssid,password,mqttusername,mqttpassword,mqttdevicename))



def check_first_boot():
    if firstBoot:
        import urequests
        import ujson
        print('in check first boot')
        response = urequests.post('http://' + mqtt_server + ':8020' + '/login/',
                                  headers={'content-type': 'application/json'},
                                  data=ujson.dumps(
                                      {"username": mqttusername, "password": mqttpassword}))
        print(response.text )
        if response.status_code == 201 or response.status_code == 200:
            response = urequests.post('http://' + mqtt_server + ':8020' + '/api/v1/device',
                                      headers={'content-type': 'application/json', "Authorization": "Token " + response.json()["key"]},
                                      data=ujson.dumps(
                                          {"desc": mqttdevicename, "state": 0, "macAddress": mac, "ipAddress": ipAddress,
                                           "deviceType": 2}))
            print(response.text)
            if response.status_code == 201 or response.status_code == 200:
                os.remove("firstBoot")
        print(str(response.status_code))
        if response.status_code == 400:
            with open('AP_mode','w+') as f:
                f.write(response.text)
                f.close()
        del response
        machine.reset()

async def manual_trigger_handler():
    manual_trigger = Pin(2,Pin.IN,Pin.PULL_UP)
    while True:
        val = 0
        while not manual_trigger.value():
            val += 1
            await asyncio.sleep(0.1)
        if val > 20:
            print('richiam manual trigger per status')
        else:
            val=0
        await asyncio.sleep(0.2)




    # main.py
# Server listening
pinIO = Pin(0, Pin.OUT)

html_page()

firstBoot=False

#import queues
queueHandler = queues.Queue()

if 'firstBoot' in os.listdir():
    firstBoot = True
print(firstBoot)
check_first_boot()

try:
    client = connect_and_subscribe()
except OSError as e:
    print(e)
    restart_and_reconnect()

# ipAddress='192.168.1.0'

# print(gc.mem_free())
thread = asyncio.get_event_loop()
thread.create_task(send_status(1))
thread.create_task(queue_handler())
thread.create_task(manual_trigger_handler())
thread.run_forever()
