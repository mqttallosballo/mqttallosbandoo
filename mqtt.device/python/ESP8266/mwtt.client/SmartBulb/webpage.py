def web_page(ssid,password,mqttusername,mqttpassword,mqttdevicename):
    import os
    html = """<html><head><title>MQTTallosbando</title> <meta name="viewport" content="width=device-width, initial-scale=1"><link rel="icon" href="data:,"><style>html{font-family: Helvetica; display:inline-block; margin: 3px auto; text-align: center;}body{background: #424242;}h1{color: #FFF; padding: 2vh;font-weight: bold;background-color: #1565C0;}input{margin-top: 4px;width: 290px;height: auto;padding: 5px;}.button{display: inline-block; border: none;border-radius: 4px; color: white; padding: 10px 50px; text-decoration: none; font-size: 16px; margin: 2px; cursor: pointer;background-color: #1565C0;}h2{padding: 3px;font-weight: bold;background-color: #C61D1D;}.selectTipe{background-color:#1565C0;color:#FFF;padding:7px;width:290px;border:none;font-size:16px;margin:4px;}</style></head><body><h1>Credenziali WiFi e Server</h1><form id="myform" action="/mqtt.ui" method="GET">"""
    #html = """<html><head><title>MQTTallosbando</title> <meta name="viewport" content="width=device-width, initial-scale=1"><link rel="icon" href="data:,"></head><body><h1>Credenziali WiFi e Server</h1><form action="/mqtt.ui" method="GET">"""
    if 'AP_mode' in os.listdir():
        with open('AP_mode','r') as f:
            html += "<h2>" + f.read() + "</h2>"
            f.close()
        os.remove('AP_mode')
        html = html + """<input type="text" name="SSID" value=\"""" + ssid + """\" placeholder="SSID" required><br>
                 <input type="text" name="password" value=\"""" + password + """\" placeholder="WiFi Password" required><br>
                 <input type="text" name="MQTTUSERNAME" value=\"""" + mqttusername + """\" placeholder="Username" required><br>
                 <input type="text" name="MQTTPASSWORD" value=\"""" + mqttpassword + """\" placeholder="Password" required><br>
                 <input type="text" name="MQTTDEVICENAME" value=\"""" + mqttdevicename + """\" placeholder="Device name" required><br>"""
    else:
        html = html + """<input type="text" name="SSID" value="" placeholder="SSID" required><br>
                        <input type="text" name="password" value="" placeholder="WiFi Password" required><br>
                        <input type="text" name="MQTTUSERNAME" value="" placeholder="Username" required><br>
                        <input type="text" name="MQTTPASSWORD" value="" placeholder="Password" required><br>
                        <input type="text" name="MQTTDEVICENAME" value="" placeholder="Device name" ><br>"""

    html += """<select class="selectTipe" name="deviceId"><option value="2">LAMPADINA</option><option value="5">SMART PLUG</option></select><br><button class="button" type="submit" value="Salva">Salva</button></form></body></html>"""
    return html
