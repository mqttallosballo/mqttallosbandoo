import uasyncio as asyncio
import machine
import os

blinking = True

async def check_is_connected(ssid, password,station):
    try:
        await asyncio.wait_for(wifi_connction(ssid, password, station),300)
    except asyncio.TimeoutError:
        if 'firstBoot' in os.listdir():
            with open('AP_mode', 'w') as f:
                f.write('Connessione WiFi non possibbile. Controllare SSID e password.')
                f.close()
        machine.reset()

async def led_blinking():
    pass
#    print('Blinko asincrono! fuck yeah')
#    while blinking:
#        #led.value(0)
#        print('18 \n')
#        print('1 \n')
#        await asyncio.sleep(0.7)

def loop_(ssid, password,station):
    loop = asyncio.get_event_loop()
    loop.create_task(led_blinking())
    loop.run_until_complete(check_is_connected(ssid, password, station))
    loop.close()
    del loop

async def wifi_connction(ssid,password,station):
    station.active(True)
    station.connect(ssid, password)
    print("ho passato la connect")
    #isConnected=False
    while station.isconnected() == False:
        await asyncio.sleep(0.2)
        #print("sto nel maledeto while isconnect")
    global ipAddress
    ipAddress = station.ifconfig()[0]
    #isConnected = True
    print(ipAddress, station.ifconfig()[1], station.ifconfig()[2], station.ifconfig()[3])
    global blinking
    blinking = False


def check_first_boot(mqtt_server, mqttusername, mqttpassword, mqttdevicename, client_id, deviceid):
    global ipAddress
    import urequests
    import ujson
    print('in check first boot')
    try:
        print('Provo u request')
        response = urequests.post('http://' + mqtt_server + ':8020' + '/login/',
                                  headers={'content-type': 'application/json'},
                                  data=ujson.dumps(
                                      {"username": mqttusername, "password": mqttpassword}))
        print(response.text )
        if response.status_code == 201 or response.status_code == 200:
            response = urequests.post('http://' + mqtt_server + ':8020' + '/api/v1/device',
                                      headers={'content-type': 'application/json', "Authorization": "Token " + response.json()["key"]},
                                      data=ujson.dumps(
                                          {"desc": mqttdevicename, "state": 0, "macAddress": client_id, "ipAddress": ipAddress,
                                           "deviceType": deviceid}))
            print(response.text)
            if response.status_code == 201 or response.status_code == 200:
                os.remove("firstBoot")
        print(str(response.status_code))
        if response.status_code == 400 or response.status_code == 103 :
            with open('AP_mode','w+') as f:
                f.write(response.text)
                f.close()
        del response
        machine.reset()
    except OSError as e:
        print('Cazzo! %s' % e)