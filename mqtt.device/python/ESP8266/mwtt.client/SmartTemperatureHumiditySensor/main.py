import queues


def sub_cb(topic, msg):
    print((topic, msg))
    #global queueHandler
    queueHandler.put_nowait(msg)
    #testo = queueHandler.get_nowait()
    #print(testo)
    # if topic == b'notification' and msg == b'received':
    # print('ESP received hello message')


def set_unit():
    print("set Unit!")
    global setUnit
    import ujson
    setUnit = jsonObj["SET_UNIT"]
    #publish_message(client, topic_sub_result, ujson.dumps(jsonObj))


async def queue_handler():
    import ujson
    while True:
        await asyncio.sleep(1)
        #print("queue handler is alive bitch!")
        if not queueHandler.empty():
            jsonObj = ujson.loads(queueHandler.get_nowait().decode('utf8').replace("'", '"'))
            fun = jsonObj["cmd"]
            if fun == "SET_UNIT":
                set_unit()





def connect_and_subscribe():
    #print("Inizio la richiesta!")
    # client = MQTTClient(client_id = client_id, server = mqtt_server, port = 58553, user = "device", password = "device", keepalive = 0, ssl=True, ssl_params={"server_side":False, "cert_reqs":ussl.CERT_NONE, "cert":cert_data, "key":key_data})
    client = MQTTClient(client_id=client_id, server=mqtt_server, port=1553, user="device", password="device",
                        keepalive=0, ssl=False)
    client.set_callback(sub_cb)
    client.connect()
    print("Connessione fatta!")
    client.subscribe(topic_sub)
    #print('Connected to %s MQTT broker, subscribed to %s topic' % (mqtt_server, topic_sub))
    return client

def restart_and_reconnect():
    print('Failed to connect to MQTT broker. Reconnecting...')
    time.sleep(10)
    machine.reset()



def html_decode(s):
    htmlCodes = (
        ("'", '&#39;'),
        ('%', '%25'),
        ('"', '&quot;'),
        ('>', '&gt;'),
        ('<', '&lt;'),
        ('&', '&amp;')
    )
    for code in htmlCodes:
        s = s.replace(code[1], code[0])
    return s


async def send_status(status):
    global humidity, temperature
    tentativi = 0
    while True:
        try:
            await asyncio.sleep(5)
            client.check_msg()
            msg = '{"state": ' + str(status) + ' , "DEV_HUMIDITY":' + str(humidity) + ', "DEV_STATE":' + str(1) + ', "DEV_TEMPERATURE":' + str(temperature) + '}'
            publish_message(client,topic_pub, msg)
            tentativi = 0
        except OSError as e:
            print("Except osrerror!")
            tentativi += 1
            print(e)
        finally:
            if tentativi == 10:
                machine.reset()


def publish_message(client,topic_pub,msg):

    try:
        print("pubblish : %s" % (topic_pub))
        print("msg : %s" % (msg))
        client.publish(topic_pub, msg)
    except OSError as e:
        print("Except osrerror!")
        print(e)




def html_page():
    try:
        import usocket as socket
    except:
        import socket
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    s.bind(('', 80))
    s.listen(5)
    while showHtml:
        import webpage
        conn, addr = s.accept()
        print('Got a connection from %s' % str(addr))
        request = str(conn.recv(1024))
        print('Content = %s' % request)
        # print('Content trunked = %s' % request[:100])
        # print(request[:100].find('SSID='))
        #response = web_page()
        if (request[:150].find('SSID=') > -1) and (request[:150].find('password=') > -1) and (
                request[:150].find('MQTTUSERNAME=') > -1) and (request[:150].find('MQTTPASSWORD=') > -1) and (
                request[:150].find('MQTTDEVICENAME=') > -1):
            import ure
            match = ure.search(
                "SSID=([^&]*)&password=([^&]*)&MQTTUSERNAME=([^&]*)&MQTTPASSWORD=([^&]*)&MQTTDEVICENAME=([^&]*)",
                request[:150])
            print(
                'crendetials to be written = {} | {} | {} | {} | {} '.format(html_decode(match.group(1)) , html_decode(str(match.group(2)).split()[0]),  html_decode(str(match.group(3)).split()[0]),  html_decode(str(match.group(4)).split()[0]),
                                                                            html_decode(str(match.group(5)).split()[0])))
            with open('wifi.txt', 'w') as f:
                f.write(html_decode(match.group(1)) + "|" + html_decode(str(match.group(2)).split()[0]) + "|" + html_decode(str(match.group(3)).split()[0]) + "|" + html_decode(str(match.group(4)).split()[0]) + "|" + html_decode(str(match.group(5)).split()[0]))
                f.close()
            #response = web_page()
            conn.write("<h1>RESETTING!!!</h1>")
            conn.close()
            machine.reset()
            #
        else:
            del request
            #conn.write("{'Content-Type': 'text/html; encoding=utf8', 'Content-Length':" + str(len(web_page())) + ", 'Connection': 'close',)} \\n")
            conn.write(webpage.web_page(ssid,password,mqttusername,mqttpassword,mqttdevicename))

async def get_temperature_and_humidity():
    global sensor,temperature, humidity
    while True:
        try:
            await asyncio.sleep(1)
            sensor.measure()
            temperature = sensor.temperature()
            humidity = sensor.humidity()
            #temperature_f = temperature * (9 / 5) + 32.0
            print('Temperature: %3.1f C' % temperature)
            #print('Temperature: %3.1f F' % temperature_f)
            print('Humidity: %3.1f %%' % humidity)
            await asyncio.sleep(6)
        except OSError as e:
            print('Failed to read sensor.')

    # main.py
# Server listening



ipAddress='1.1.1.1'

html_page()

temperature = 0
humidity = 0

#import queues
queueHandler = queues.Queue()

if 'firstBoot' in os.listdir():
    from ESPtool import check_first_boot
    check_first_boot(mqtt_server, mqttusername, mqttpassword, mqttdevicename, client_id, 8)
    #firstBoot = True

try:
    client = connect_and_subscribe()
except OSError as e:
    print(e)
    restart_and_reconnect()

from dht import DHT11
sensor = DHT11(Pin(2))

import uasyncio as asyncio

thread = asyncio.get_event_loop()
thread.create_task(send_status(1))
thread.create_task(queue_handler())
thread.create_task(get_temperature_and_humidity())
thread.run_forever()

