#
# Istruzione per caricare .bin su esp 8285
# esptool.py --port /dev/ttyUSB0 --baud 115200 write_flash -fm dout -fs 8m -ff 40m 0x00000 <NAME_BIN> --verify
#
import os
import time
from umqttsimple import MQTTClient
import machine
from machine import Pin
import network
import esp
esp.osdebug(None)
import gc


def reset_file():
    if 'wifi.txt' in os.listdir():
        os.remove('wifi.txt')
    with open('firstBoot', 'w+') as f:
        f.close()


def chekc_wifi_credentials(file):
    #ssid = password = mqttusername = mqttpassword = mqttdevicename = " "
    print('Proverò a legger il file : %s' % (file))
    try:
        temp=''
        with open(file, "r") as f:
            temp = f.read()
            f.close()
        print('wifi cred:' + temp)
        #ssid, password, mqttusername, mqttpassword, mqttdevicename = temp.split('|')
        # password = temp.split('|')[1]
        if 'AP_mode' in os.listdir():
            AP_mode = True
            os.remove(file)
        else:
            AP_mode = False
    except:
        AP_mode = True
    #return ssid, password, mqttusername, mqttpassword, mqttdevicename, AP_mode
    if len(temp) > 0:
        return temp.split('|')[0], temp.split('|')[1], temp.split('|')[2] , temp.split('|')[3], temp.split('|')[4], temp.split('|')[5], AP_mode
    else:
        return "", "", "", "", "", "", AP_mode


def button_reset():
    resetButton = Pin(0, Pin.IN, Pin.PULL_UP)
    finished=0
    pushed=0
    while finished <= 24:
        led.value(not led.value())
        pushed += resetButton.value()
        time.sleep(0.100)
        finished += 1
        #led.value(1)
    if int(pushed) < 12:
        led.value(1)
        led.value(0)
        reset_file()
        finished = 0
        time.sleep(1.5)
        led.value(1)
#        while finished < 1000:
#            print('antonio|pietro^2')
#            finished += 1
        machine.reset()
    del finished, pushed, resetButton
    led.value(1)



station = network.WLAN(network.STA_IF)
station.active(False)
ap_if = network.WLAN(network.AP_IF)
ap_if.active(False)

#global led
led = Pin(13, Pin.OUT)
#led.value(0)

button_reset()

#blinking = True
'''
password = " "
ssid = " "
mqttusername = " "
mqttpassword = " "
mqttdevicename = " "
ipAddress = " "
'''
#time.sleep(0.500)
mqtt_server = 'YOUR_SERVER'

try:
    client_id = ''
    for ot in list(network.WLAN().config('mac')): h = hex(ot); client_id += h[2] + h[3] + ':'
except:
    client_id="00:00:00:00:00:33:"

client_id = client_id[:-1]

topic_sub_result = b'devices/' + client_id + '/result'
topic_sub = b'devices/' + client_id + '/command'
topic_pub = b'devices/' + client_id + '/state'


ssid, password, mqttusername, mqttpassword, mqttdevicename,deviceid, AP_mode = chekc_wifi_credentials('wifi.txt')
print('imposto AP? NON trovato wifi.txt? : %s' % AP_mode)
showHtml=False

if not AP_mode:
    import ESPtool
#    print('ho trovato il file con le credenziali del wifi')
    ESPtool.loop_(ssid, password, station, led)
else:
#    print("imposto il mio AP")
    ap_if.active(True)
    ap_if.config(essid="MQTTallosbando",password="11111111")
    showHtml=True
    led.value(0)

#Info per memoria
print('Memoria allocata = ' + str(gc.mem_alloc()))
print('Memoria libera   = ' + str(gc.mem_free()))

print('fine boot')
gc.collect()
print('Memoria libera   = ' + str(gc.mem_free()))
