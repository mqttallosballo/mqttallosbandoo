print('Memoria libera   = ' + str(gc.mem_free()))
if not 'AP_mode' in os.listdir():
    del ssid, password

print('Entro nel main!')

import queues

def sub_cb(topic, msg):
    print((topic, msg))
    #global queueHandler
    queueHandler.put_nowait(msg)
    #testo = queueHandler.get_nowait()
    #print(testo)
    # if topic == b'notification' and msg == b'received':
    # print('ESP received hello message')

def set_value(jsonObj):
    print("set value!")
    import ujson
    global topic_sub_result, manualTrigger
    manualTrigger=0
    pinIO.value(int(jsonObj["value"]))
    if(pinIO.value() == 1):
        if 'pinHigh' in os.listdir():
            os.remove('pinHigh')
    else:
        f = open('pinHigh', 'w')
        f.close
    jsonObj["status"] = 0
    publish_message(client, topic_sub_result, ujson.dumps(jsonObj))

def get_value(jsonObj):
    import ujson
    print("get value!")
    global topic_sub_result
    jsonObj["result"] = pinIO.value()
    jsonObj["status"] = 0
    publish_message(client, topic_sub_result, ujson.dumps(jsonObj))

def set_state(jsonObj):
    print("set state!")
    pass

def get_state(jsonObj):
    print("get state!")
    pass

async def queue_handler():
    import ujson
    while True:
        await asyncio.sleep(1)
        #print("queue handler is alive bitch!")
        if not queueHandler.empty():
            jsonObj = ujson.loads(queueHandler.get_nowait().decode('utf8').replace("'", '"'))
            fun = jsonObj["cmd"]
            if fun == "SET_VALUE":
                set_value(jsonObj)
            if fun == "GET_VALUE":
                get_value(jsonObj)
            if fun == "GET_STATE":
                get_state(jsonObj)
            if fun == "SET_STATE":
                set_state(jsonObj)

def connect_and_subscribe():
    #print("Inizio la richiesta!")
    # client = MQTTClient(client_id = client_id, server = mqtt_server, port = 58553, user = "device", password = "device", keepalive = 0, ssl=True, ssl_params={"server_side":False, "cert_reqs":ussl.CERT_NONE, "cert":cert_data, "key":key_data})
    client = MQTTClient(client_id=client_id, server=mqtt_server, port=1553, user="device", password="device",
                        keepalive=0, ssl=False)
    client.set_callback(sub_cb)
    client.connect()
    print("Connessione fatta!")
    client.subscribe(topic_sub)
    #print('Connected to %s MQTT broker, subscribed to %s topic' % (mqtt_server, topic_sub))
    return client

def restart_and_reconnect():
    print('Failed to connect to MQTT broker. Reconnecting...')
    time.sleep(10)
    machine.reset()

async def send_status(status):
    global manual_trigger
    while True:
        try:
            client.check_msg()
            msg = '{"state": ' + str(status) + ' , "DEV_VALUE":' + str(pinIO.value()) + ', "DEV_STATE":' + str(1) + ', "MANUAL_TRIGGER":' + str(manualTrigger) + '}'
            publish_message(client,topic_pub, msg)
            await asyncio.sleep(5)
        except OSError as e:
            print("Except osrerror!")
            print(e)

def publish_message(client,topic_pub,msg):
    global led
    led.value(1)
    try:
        led.value(0)
        print("pubblish : %s" % (topic_pub))
        print("msg : %s" % (msg))
        client.publish(topic_pub, msg)
    except OSError as e:
        print("Except osrerror!")
        print(e)
    finally:
        led.value(1)

def html_page():
    try:
        import usocket as socket
    except:
        import socket
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    s.bind(('', 80))
    s.listen(5)
    while showHtml:
        import webpage
        conn, addr = s.accept()
        print('Got a connection from %s' % str(addr))
        request = str(conn.recv(1024))
        print('Content = %s' % request)
        if (request[:150].find('SSID=') > -1) and (request[:150].find('password=') > -1) and (
                request[:150].find('MQTTUSERNAME=') > -1) and (request[:150].find('MQTTPASSWORD=') > -1) and (
                request[:150].find('MQTTDEVICENAME=') > -1) and (
                request[:150].find('deviceId=') > -1):
            import ure
            match = ure.search(
                "SSID=([^&]*)&password=([^&]*)&MQTTUSERNAME=([^&]*)&MQTTPASSWORD=([^&]*)&MQTTDEVICENAME=([^&]*)&deviceId=([^&]*)",
                request[:150])
            print(
                'crendetials to be written = {} | {} | {} | {} | {} | {}'.format(webpage.html_decode(match.group(1)) , webpage.html_decode(str(match.group(2)).split()[0]),  webpage.html_decode(str(match.group(3)).split()[0]),  webpage.html_decode(str(match.group(4)).split()[0]),
                                                                            webpage.html_decode(str(match.group(5)).split()[0]), webpage.html_decode(str(match.group(6)).split()[0])))
            with open('wifi.txt', 'w') as f:
                f.write(webpage.html_decode(match.group(1)) + "|" + webpage.html_decode(str(match.group(2)).split()[0]) + "|" + webpage.html_decode(str(match.group(3)).split()[0]) + "|" + webpage.html_decode(str(match.group(4)).split()[0]) + "|" + webpage.html_decode(str(match.group(5)).split()[0]) + "|" + webpage.html_decode(str(match.group(6)).split()[0]))
                f.close()
            conn.write("<h1>RESETTING!!!</h1>")
            conn.close()
            machine.reset()
        else:
            del request
            conn.write(webpage.web_page(ssid,password,mqttusername,mqttpassword,mqttdevicename))

async def manual_trigger_handler():
    global manualTrigger
    manual_trigger = Pin(0,Pin.IN,Pin.PULL_UP)
    #manual_trigger = Pin(14, Pin.IN, Pin.PULL_UP)
    print('button value %s' % manual_trigger.value() )
    while True:
        val = 0
        while not manual_trigger.value():
            val += 1
            #await asyncio.sleep(0.1)
        if val > 10:
            print('Richiamo manual trigger per status')
            manualTrigger=1
            pinIO.value(not int(pinIO.value()))
            await asyncio.sleep(0.3)
            if(pinIO.value() == 1):
                if 'pinHigh' in os.listdir():
                    os.remove('pinHigh')
            else:
                f = open('pinHigh', 'w')
                f.close
        else:
            val=0
        await asyncio.sleep(0.2)

async def reboot_to_connect():
    #attesa di 30 minuti per ritentare la connesione
    await asyncio.sleep(1800)
    machine.reset()

pinIO = Pin(12, Pin.OUT)
#pinIO = Pin(0, Pin.OUT)
if 'pinHigh' in os.listdir():
    value = 0
else:
    value = 1
pinIO.value(value)
del value

ipAddress='1.1.1.1'

html_page()

#firstBoot=False

manualTrigger = 0

#import queues
queueHandler = queues.Queue()

if 'firstBoot' in os.listdir():
    from ESPtool import check_first_boot
    check_first_boot(mqtt_server, mqttusername, mqttpassword, mqttdevicename, client_id, deviceid)
    #firstBoot = True

if not 'timeOutWiFi' in os.listdir():
    try:
        client = connect_and_subscribe()
    except OSError as e:
        print(e)
        restart_and_reconnect()

# ipAddress='192.168.1.0'

# print(gc.mem_free())
import uasyncio as asyncio
thread = asyncio.get_event_loop()
if not 'timeOutWiFi' in os.listdir():
    thread.create_task(send_status(1))
    thread.create_task(queue_handler())
else:
    thread.create_task(reboot_to_connect())
    os.remove('timeOutWiFi')
thread.create_task(manual_trigger_handler())
thread.run_forever()

