MQTT devices based on ESP8266

please use "mqttallosbando_ESP8266_1MB_v1.bin" image file.

refer to following guide for further information about esp 8266 flashing

https://nodemcu.readthedocs.io/en/master/flash/

Use esptool.py for firmware flash:

COMPLETE ERASE FIRMWARE
esptool.py -p /dev/ttyUSB0 erase_flash

ESP8285
esptool.py --port /dev/ttyUSB0 --baud 115200 write_flash -fm dout -fs 8m -ff 40m 0x00000 <NAME_BIN> --verify

ESP8266
esptool.py --p /dev/ttyUSB0 write_flash 0x0000000 <NAME_BIN>