import Vue from 'vue'
import App from './App.vue'
import './registerServiceWorker'
import router from './router'
import store from './store'
import i18n from '@/plugins/i18n';
import vuetify from './plugins/vuetify';
import Vue2TouchEvents from 'vue2-touch-events'
import * as firebase from "firebase";

import api from '@/modules/api/index';

Vue.use(Vue2TouchEvents)

Vue.config.productionTip = false

Vue.prototype.$appSettings = {
  title: 'MQTT UI',
  refreshInterval: 0
}


const config = {
  apiKey: "",
  authDomain: "",
  databaseURL: "",
  projectId: "",
  storageBucket: "",
  messagingSenderId: "",
  appId: "",
  measurementId: ""
};
if (firebase.messaging.isSupported()){

  firebase.initializeApp(config);

  const messaging = firebase.messaging();

  messaging.usePublicVapidKey(""); // 1. Generate a new key pair

  // Request Permission of Notifications
  messaging.requestPermission().then(() => {
    console.log('Notification permission granted.');

    // Get Token
    messaging.getToken().then((token) => {
      const registerNotifTokenURL = '/api/v1/register-token/'
      const payload = {
        registration_id: token,
        type: 'web'
      }
      api.myAxiosInstance.post(registerNotifTokenURL, 
        payload,
        { headers: { 'Authorization': 'Token '+localStorage.getItem('user-token') || '', }})
        .then((response) => {
          console.log('Successfully saved notification token!')
        })
        .catch((error) => {
          console.log('Error: could not save notification token')
          if (error.response) {
            console.log(error.response.status)
            if (error.response.data.registration_id) {
              for (let err of error.response.data.registration_id) {
              }
            } else {
              console.log('No reason returned by backend')
            }
          } else if (error.request) {
            console.log('A network error occurred.')
          } else {
            console.log(error.message)
          }
        })
    })
  }).catch((err) => {
    console.log('Unable to get permission to notify.', err);
  });
}
new Vue({
  router,
  store,
  i18n,  
  vuetify,
  render: h => h(App)
}).$mount('#app')
