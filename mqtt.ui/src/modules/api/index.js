import axios from 'axios'

const httpSSl = require('https');

const myAxiosInstance = axios.create({
    baseURL: 'https://<agent ip>:<agent port>',
    timeout: 0,
    cache: false,
    headers: {
        'Content-Type': 'application/json'
    },
    httpsAgent: new httpSSl.Agent({  
       rejectUnauthorized: false
     })
})

export default {
    myAxiosInstance
}