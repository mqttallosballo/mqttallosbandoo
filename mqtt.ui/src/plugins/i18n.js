import Vue from 'vue'
import VueI18n from 'vue-i18n'
import msgIt from '@/plugins/locales/it.js'
import msgEn from '@/plugins/locales/en.js'

Vue.use(VueI18n)

const i18n = new VueI18n({
    locale: 'it',
    fallbackLocale: 'en',
    localeDir: 'locales',
    enableInSFC: true,
    messages: {
        it :msgIt,
        en :msgEn
    },
    missing: (locale, key) =>
    {
        const regex = /[$\w]+[.]\S+[.](.*)/;
        const subst = `$1`;
        return `${key.replace(regex, subst)}`;
    },
});

export default i18n
