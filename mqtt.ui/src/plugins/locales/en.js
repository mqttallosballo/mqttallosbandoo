export default { 
  "$app": {
    "close": "Close",
    "loadingText": "Loading...",
    "itemsPerPageText": "Rows per page:",
    "sortBy": "Order by",
    "dataFooter": {
      "itemsPerPageText": "Number of records",
      "itemsPerPageAll": "All",
      "nextPage": "Next Page",
      "prevPage": "Previus page",
      "firstPage": "First page",
      "lastPage": "Last page",
      "pageText": "{0}-{1} of {2}"
    },
    "noDataText": "No elements",

    "menu": {
      "dashboard": "Dashboard",
      "devices": "Devices",
      "settings": "Settings",
      "logout": "Exit"
    },

    "dashboard": {
        "title" : "Dashboard",
        "subtitle" : "",
        "mac": "MAC",
        "lastUpdate": "Last Update",
        "deviceState0" : "Not connected",
        "deviceState1" : "Connected",
        "deviceState2" : "Disconnected",
        "deviceState3" : "Error"
    },

    "login": {
      "title": "Login",
      "subtitle": ""
    },

    "devices" : {
      "title" : "Devices",
      "subtitle" : "",

      "DEV_VALUE": "UPDATE DEVICE VALUE",
      "DEV_STATE": "UPDATE DEVICE STATE",
      "MANUAL_TRIGGER": "MANUAL TRIGGER",
      "DEV_TEMPERATURE": "UPDATE TEMPERATURE ",
      "DEV_HUMIDITY": "UPDATE HUMIDITY",
      "DEV_INTENSITY": "UPDATE ITENSITY'",        

      "SET_VALUE": "SET VALUE",
      "SET_STATE": "SET STATE",
      "SET_INTENSITY": "SET INTENSITY'"
    },

    "command":{
      "unavailable": "Not available",
      "queue": "In queue",
      "processing": "Processing",
      "completed": "Completed",
      "error": "Error"
    },

    "event":{
      "unavailable": "Not available",
      "completed": "Completed",
      "error": "Error"
    },      

    "categories" : {
      "pir" : "PIR",
      "led" : "Led",
      "sensor" : "Humidity and Temperture sensor",
      "cam" : "Camera",
      "smartPlug" : "Smart plug",
      "ir" : "IR"
    },

    "commands" : {
        "GET_STATE" : "Get state",
        "SET_STATE" : "Set sate",
        "GET_VALUE" : "Get value",
        "SET_VALUE" : "Set value",
        "GET_MANUAL_TRIGGER" : "Get manual trigger",
        "TAKE_PIC" : "Snap a picture",
        "GET_PIC" : "Get a picture",
        "DEVICE_IR_MODEL" : "IR device model",
        "GET_DEVICE_IR_INSTR" : "Get IR command isntructions",
        "SET_DEVICE_IR_INSTR" : "Set IR command isntructions"
    }
  }
}