export default { 
    "$app": {
      "close": "Chiudi",
      "loadingText": "Caricamento in corso...",
      "itemsPerPageText": "Righe per pagina:",
      "sortBy": "Ordina per",
      "dataFooter": {
        "itemsPerPageText": "Elementi per pagina:",
        "itemsPerPageAll": "Tutti",
        "nextPage": "Pagina seguente",
        "prevPage": "Pagina precedente",
        "firstPage": "Pagina prima",
        "lastPage": "Pagina ultima",
        "pageText": "{0}-{1} di {2}"
      },
      "noDataText": "Nessun elemento disponibile",
  
      "menu": {
        "dashboard": "Dashboard",
        "devices": "Dispositivi",
        "settings": "Impostazioni",
        "logout": "Esci"
      },

      "dashboard": {
          "title" : "Dashboard",
          "subtitle" : "",
          "mac": "MAC",
          "lastUpdate": "Ultimo aggiornamento",          
          "deviceState0" : "Non connesso",
          "deviceState1" : "Connesso",
          "deviceState2" : "Disconnesso",
          "deviceState3" : "Errore"
      },

      "login": {
        "title": "Login",
        "subtitle": ""
      },

      "devices" : {
        "title" : "Dispositivi",
        "subtitle" : "",

        "DEV_VALUE": "AGG. VALORE ATTUALE",
        "DEV_STATE": "AGG. STATO ATTUALE",
        "MANUAL_TRIGGER": "ATTIVAZIONE MANUALE",
        "DEV_TEMPERATURE": "AGG. TEMPERATURA",
        "DEV_HUMIDITY": "AGG. UMIDITA'",
        "DEV_INTENSITY": "AGG. VALORE INTENSITA'",        

        "SET_VALUE": "SET VALORE",
        "SET_STATE": "SET STATO",
        "SET_INTENSITY": "SET INTENSITA'"
      },

      "command":{
        "unavailable": "Non disponibile",
        "queue": "In coda",
        "processing": "In lavorazione",
        "completed": "Completato",
        "error": "Errore"
      },

      "event":{
        "unavailable": "Non disponibile",
        "completed": "Completato",
        "error": "Errore"
      },

      "history":{
        "title": "History",
        "date": "Filtra per data",
        "time": "Orario"
      },

      "categories" : {
        "pir" : "PIR",
        "led" : "Led",
        "sensor" : "Sensore umidità o similari",
        "cam" : "Camera",
        "smartPlug" : "Presa intelligente",
        "ir" : "IR"
      },

      "commands" : {
          "GET_STATE" : "Leggi Stato",
          "SET_STATE" : "Imposta Stato",
          "GET_VALUE" : "Leggi Valore",
          "SET_VALUE" : "Imposta Valore",
          "GET_MANUAL_TRIGGER" : "Attivato manualmente",
          "TAKE_PIC" : "Scatta Foto",
          "GET_PIC" : "Scarica Foto",
          "DEVICE_IR_MODEL" : "Modello Device IR",
          "GET_DEVICE_IR_INSTR" : "Ottieni set istruzioni IR",
          "SET_DEVICE_IR_INSTR" : "Imposta set istruzioni IR"
      }
    }
}