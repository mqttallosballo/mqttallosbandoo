import Vue from 'vue'
import Vuetify from 'vuetify/lib'
import colors from 'vuetify/lib/util/colors'
import '@fortawesome/fontawesome-free/css/all.css'

Vue.use(Vuetify);

export default new Vuetify({
    icons: {
        iconfont: 'fa',
        values: {
            user: 'fa-user-secret',
            logout: 'fa-sign-out-alt',
            home: 'fa-home',
            dashboard: 'fa-columns',
            device: 'mdi-devices',
            group:'fa-layer-group',
            network: 'fa-network-wired',
            ipnetwork: 'mdi-ip-network',
            subnet: 'mdi-check-network',
            gateway: 'fa-globe',
            dns: 'fa-random',
            yes: 'fa-check',
            no: 'fa-times',
            sendCommand: 'fa-cloud-upload-alt',
            waitCommand: 'fa-cloud-download-alt',
            deviceType1: 'fa-person-booth',
            deviceType2: 'fa-lightbulb',
            deviceType3: 'mdi-leak',
            deviceType4: 'mdi-camera-wireless',
            deviceType5: 'fa-plug',
            deviceType6: 'mdi-remote',
            deviceType8: 'fa-thermometer-quarter',
            deviceType9: 'fa-bullseye',
            password: 'fa-key',
            show: 'mdi-eye',
            hide: 'mdi-eye-off'
        },
    },

    theme: {
        dark: false,

        themes: {

            dark: {
                primary: colors.blue.darken1,
                tertiary: colors.blue,
                success: colors.green,
                warning: colors.orange,
                error: colors.red,
            },

            light: {
                primary: colors.blue.darken3,
                secondary: colors.grey.accent3,
                tertiary: colors.blue,
                success: colors.green,
                warning: colors.orange,
                error: colors.red,
            },
            options: {
                customProperties: true,
              },
        },
    },
});
