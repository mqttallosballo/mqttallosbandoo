//AUTH
export const AuthReq = 'AuthReq';
export const AuthRequestSucc = 'AuthRequestSucc';

export const AuthErr = 'AuthErr';
export const AuthSignOff = 'AuthSignOff';

//COMMAND
export const CmdsReq = 'CmdsReq';
export const CmdsReqSucc = 'CmdsReqSucc';

export const CmdReq = 'CmdReq';
export const CmdReqSucc = 'CmdReqSucc';

export const CmdErr = 'CmdErr';

//DEVICE
export const DevReq = 'DevReq';
export const DevReqSucc = 'DevReqSucc';

export const EditDevReq = 'EditDevReq';
export const EditDevReqSucc = 'EditDevReqSucc';

export const DevGroupReq = 'DevGroupReq';
export const DevGroupsReqSucc = 'DevGroupsReqSucc';

export const DevErr = 'DevErr';

//EVENT
export const EventsReq = 'EventsReq';
export const EventsReqSucc = 'EventsReqSucc';

export const EventReq = 'EventReq';
export const EventReqSucc = 'EventReqSucc';

export const EventErr = 'EventErr';

//GROUP
export const GroupsReq = 'GroupsReq'
export const GroupsReqSucc = 'GroupsReqSucc'

export const GroupReq = 'GroupReq'
export const GroupReqSucc = 'GroupReqSucc'

export const EditGroupReq = 'EditGroupReq'
export const EditGroupReqSucc = 'EditGroupReqSucc'

export const AddGroupReq = 'AddGroupReq'
export const AddGroupReqSucc = 'AddGroupReqSucc'

export const GroupErr = 'GroupErr'

//JOIN
export const JoinReq = 'JoinReq'
export const JoinReqSucc = 'JoinReqSucc'

export const AddJoinReq = 'AddJoinReq'
export const AddJoinReqSucc = 'AddJoinReqSucc'

export const DelJoinReq = 'DelJoinReq'
export const DelJoinReqSucc = 'DelJoinReqSucc'

export const JoinErr = 'GroupErr'

//HISTORY
export const HistoryReq = 'HistoryReq'
export const HistoryReqSucc = 'HistoryReqSucc'

export const  HistoryErr = 'HistoryErr'

//USER
export const UserReq = 'UserReq';
export const UserReqSucc = 'UserReqSucc';

export const UserErr = 'UserErr';
