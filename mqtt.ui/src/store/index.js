import Vue from 'vue'
import Vuex from 'vuex'
import auth from '@/store/modules/auth'
import user from '@/store/modules/user'
import device from '@/store/modules/device'
import command from '@/store/modules/command'
import event from '@/store/modules/event'
import group from '@/store/modules/group'
import join from '@/store/modules/join'
import history from '@/store/modules/history'

Vue.use(Vuex)

const debug = process.env.NODE_ENV !== 'production'

export default new Vuex.Store({
  modules: {
    auth,
    user,
    device,
    command,
    event,
    group,
    join,
    history,
    strict: debug,
  }
})
