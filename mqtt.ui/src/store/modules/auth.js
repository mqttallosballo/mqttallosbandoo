import
{
    AuthReq, AuthRequestSucc,
    AuthSignOff,
    AuthErr,

    UserReq

} from '@/store/actions.js';

import api from '@/modules/api/index';
 
const state = {
    status: '',
    token: localStorage.getItem('user-token') || '',
}

const getters = {
    isLoggedin: state => !!state.token,
    authStatus: state => state.status
}

const actions =
{
    [AuthReq]: ({ commit, dispatch }, credentials) => 
    {
        return new Promise((resolve, reject) =>
        {

            commit(AuthReq);
            api.myAxiosInstance
                .post('/login/', credentials)
                .then(resp =>
                {
                   
                    localStorage.setItem('user-token', resp.data.key);
                    commit(AuthRequestSucc, resp.data);
                    dispatch(UserReq);
                    resolve(resp);
                })
                .catch(err =>
                {
                    commit(AuthErr, err.response);
                    localStorage.removeItem('user-token');
                    reject(err.response);
                });
        });
    },

    [AuthSignOff]: ({ commit }) =>
    {

        return new Promise((resolve) =>
        {
            commit(AuthSignOff);
            localStorage.removeItem('user-token');
            resolve();
        });
    }
}

const mutations =
{
    [AuthReq]: (state) =>
    {
        state.status = 'loading';
    },

    [AuthRequestSucc]: (state, payload) =>
    {
        state.status = 'success';
        state.token = payload.key;
    },

    [AuthErr]: (state) =>
    {
        state.status = 'error';
    },

    [AuthSignOff]: (state) =>
    {
        state.token = '';
    }
}

export default {
    state,
    getters,
    actions,
    mutations,
}
