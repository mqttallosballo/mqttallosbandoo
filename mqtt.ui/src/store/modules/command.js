import
{
    CmdsReq, CmdsReqSucc,
    CmdReq, CmdReqSucc,
    CmdErr

} from '@/store/actions.js';
 
import api from '@/modules/api/index';

const state = {
    commands: [],
    command: {},
    errors : "",
    status: ""
}

const getters = {
    getCommands: state => state.commands,
    //errors: state => state.errors
}

const actions =
{
    [CmdsReq]: ({ commit, dispatch }, macAddress) => 
    {
        return new Promise((resolve, reject) =>
        {

            commit(CmdsReq);
            api.myAxiosInstance
                .get('/api/v1/device/'+macAddress+'/command', 
                    { headers: { 'Authorization': 'Token '+localStorage.getItem('user-token') || '', }})
                .then(resp =>
                {
                    commit(CmdsReqSucc, resp.data);
                    resolve(resp);
                })
                .catch(err =>
                {
                    commit(CmdErr, err);
                    reject(err);
                });
        });
    },
    [CmdReq]: ({ commit, dispatch }, data) => 
    {
        return new Promise((resolve, reject) =>
        {
            commit(CmdReq);
            let payload = {
                "status" : 2,
                "value" : data.value
            }

            api.myAxiosInstance
                .patch('/api/v1/device/'+data.macAddress+'/command/'+data.cmd, 
                    payload, 
                    { headers: { 'Authorization': 'Token '+localStorage.getItem('user-token') || ''}})
                .then(resp =>
                {
                    commit(CmdReqSucc, resp.data);
                    resolve(resp);
                })
                .catch(err =>
                {
                    commit(CmdErr, err);
                    reject(err);
                });
        });
    }    
}

const mutations =
{
    [CmdsReq]: (state) =>
    {
        state.status = 'loading';
    },

    [CmdsReqSucc]: (state, payload) =>
    {
        state.status = 'success';
        state.commands = payload;
    },

    [CmdReq]: (state) =>
    {
        state.status = 'loading';
    },

    [CmdReqSucc]: (state, payload) =>
    {
        state.status = 'success';
        state.command = payload;
    },

    [CmdErr]: (state, err) =>
    {
        state.status = 'error';
        state.errors = err;
    }
}

export default {
    state,
    getters,
    actions,
    mutations,
}
