import
{
    DevReq, DevReqSucc,
    DevGroupReq, DevGroupsReqSucc,
    EditDevReq, EditDevReqSucc,
    DevErr

} from '@/store/actions.js';

import api from '@/modules/api/index';

const state = {
    devices: [],
    devicesGroup: [],
    errors : "",
    status: ""
}

const getters = {
    devices: state => state.devices,
    getDeviceByMacAddress: (state) => (macAddress) => {
        if (Object.keys(state.devices).length != 0) {
            return state.devices.find(device => device.macAddress === macAddress)
        }
    },
    isDeviceLoading: state => {
        if(state.status == 'success'){
            return false
        }else{
            return true
        }
    }
}

const actions =
{
    [DevReq]: ({ commit, dispatch }) => 
    {
        return new Promise((resolve, reject) =>
        {

            commit(DevReq);
            api.myAxiosInstance
                .get('/api/v1/device', 
                    { headers: { 'Authorization': 'Token '+localStorage.getItem('user-token') || '', }})
                .then(resp =>
                {
                    commit(DevReqSucc, resp.data);
                    resolve(resp);
                })
                .catch(err =>
                {
                    commit(DevErr, err);
                    reject(err);
                });
        });
    },
    [DevGroupReq]: ({ commit, dispatch }, group) => 
    {
        return new Promise((resolve, reject) =>
        {
            commit(DevGroupReq);
            api.myAxiosInstance
                .get('/api/v1/device?group=' + group, 
                    { headers: { 'Authorization': 'Token '+localStorage.getItem('user-token') || '', }})
                .then(resp =>
                {
                    commit(DevGroupsReqSucc, resp.data);
                    resolve(resp);
                })
                .catch(err =>
                {
                    commit(DevErr, err);
                    reject(err);
                });
        });
    },
    [EditDevReq]: ({ commit, dispatch }, data) => 
    {
        return new Promise((resolve, reject) =>
        {

            
            commit(EditDevReq);

            let token = localStorage.getItem('user-token');

            let url = '/api/v1/device/'+data.macAddress;

            let payload = {
                "desc" : data.desc
            }

            api.myAxiosInstance
                .patch('/api/v1/device/'+data.macAddress, 
                    payload, 
                    { headers: { 'Authorization': 'Token '+localStorage.getItem('user-token') || '', }})
                .then(resp =>
                {
                    dispatch(DevReq);
                    resolve(resp);
                })
                .catch(err =>
                {
                    commit(DevErr, err);
                    reject(err);
                });
        });
    },    

}

const mutations =
{
    [DevReq]: (state) =>
    {
        state.status = 'loading';
    },

    [DevReqSucc]: (state, payload) =>
    {
        state.status = 'success';
        state.devices = payload;
    },

    [DevGroupReq]: (state) =>
    {
        state.status = 'loading';
    },  

    [DevGroupsReqSucc]: (state, payload) =>
    {
        state.status = 'success';
        state.devicesGroup = payload;
    },    

    [EditDevReq]: (state) =>
    {
        state.status = 'loading';
    },

    [DevErr]: (state, err) =>
    {
        state.status = 'error';
        state.errors = err;
    }
}

export default {
    state,
    getters,
    actions,
    mutations,
}
