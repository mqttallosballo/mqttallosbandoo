import
{
    EventsReq, EventsReqSucc,
    EventReq, EventReqSucc,
    EventErr
 
} from '@/store/actions.js';

import api from '@/modules/api/index';

const state = {
    events: [],
    event: {},
    errors : "",
    status: ""
}

const getters = {
    getEvents: state => state.events,
}

const actions =
{
    [EventsReq]: ({ commit, dispatch }, macAddress) => 
    {
        return new Promise((resolve, reject) =>
        {
            commit(EventsReq);
            api.myAxiosInstance
                .get('/api/v1/device/'+macAddress+'/event', 
                    { headers: { 'Authorization': 'Token '+localStorage.getItem('user-token') || '', }})
                .then(resp =>
                {
                    commit(EventsReqSucc, resp.data);
                    resolve(resp);
                })
                .catch(err =>
                {
                    commit(EventErr, err);
                    reject(err);
                });
        });
    },
    [EventReq]: ({ commit, dispatch }, data) => 
    {
        return new Promise((resolve, reject) =>
        {
            commit(EventReq);
            let payload = {
                "id" : data.cmdId,
                "desc" : data.desc,
                "status" : 2
            }

            api.myAxiosInstance
                .patch('/api/v1/device/'+data.macAddress+'/event/'+data.cmdId, 
                    payload, 
                    { headers: { 'Authorization': 'Token '+localStorage.getItem('user-token') || '', }})
                .then(resp =>
                {
                    commit(EventReqSucc, resp.data);
                    resolve(resp);
                })
                .catch(err =>
                {
                    commit(EventErr, err);
                    reject(err);
                });
        });
    }    
}

const mutations =
{
    [EventsReq]: (state) =>
    {
        state.status = 'loading';
    },

    [EventsReqSucc]: (state, payload) =>
    {
        state.status = 'success';
        state.events = payload;
    },

    [EventReq]: (state) =>
    {
        state.status = 'loading';
    },

    [EventReqSucc]: (state, payload) =>
    {
        state.status = 'success';
        state.event = payload;
    },

    [EventErr]: (state, err) =>
    {
        state.status = 'error';
        state.errors = err;
    }
}

export default {
    state,
    getters,
    actions,
    mutations,
}
