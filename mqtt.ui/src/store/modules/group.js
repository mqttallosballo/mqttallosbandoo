import {
    GroupsReq, GroupsReqSucc, 
    GroupReq, GroupReqSucc,
    EditGroupReq, EditGroupReqSucc,
    AddGroupReq, AddGroupReqSucc, 
    GroupErr
} from '@/store/actions.js';

import api from '@/modules/api/index'; 

const state = {
    groups: [],
    errors : "",
    status: ""
}

const getters = {
    groups: state => state.groups,
    isGroupLoading: state => {
        if(state.status == 'success'){
            return false
        }else{
            return true
        }
    }    
}

const actions =
{
    [GroupsReq]: ({ commit, dispatch }) => 
    {
        return new Promise((resolve, reject) =>
        {
            commit(GroupsReq);
            api.myAxiosInstance
                .get('/api/v1/group', 
                    { headers: { 'Authorization': 'Token '+localStorage.getItem('user-token') || '', }})
                .then(resp =>
                {
                    commit(GroupsReqSucc, resp.data);
                    resolve(resp);
                })
                .catch(err =>
                {
                    commit(GroupErr, err);
                    reject(err);
                });
        });
    },
    [GroupReq]: ({ commit, dispatch }, data) => 
    {
        return new Promise((resolve, reject) =>
        {
            commit(GroupReq);
        });
    },
    [EditGroupReq]: ({ commit, dispatch }, data) => 
    {
        return new Promise((resolve, reject) =>
        {
            commit(EditGroupReq);
            let payload = {
                "desc" : data.desc
            }
            api.myAxiosInstance
                .patch('/api/v1/group/'+data.id, 
                    payload, 
                    { headers: { 'Authorization': 'Token '+localStorage.getItem('user-token') || '', }})
                .then(resp =>
                {
                    dispatch(GroupsReq);
                    resolve(resp);
                })
                .catch(err =>
                {
                    commit(GroupErr, err);
                    reject(err);
                });
        });
    },
    [AddGroupReq]: ({ commit, dispatch }, data) => 
    {
        return new Promise((resolve, reject) =>
        {
            commit(AddGroupReq);
            let payload = {
                "desc" : data.desc
            }
            api.myAxiosInstance
                .post('/api/v1/group', 
                    payload, 
                    { headers: { 'Authorization': 'Token '+localStorage.getItem('user-token') || '', }})
                .then(resp =>
                {
                    dispatch(GroupsReq);
                    resolve(resp);
                })
                .catch(err =>
                {
                    commit(GroupErr, err);
                    reject(err);
                });
        });
    }            
}

const mutations =
{
    [GroupsReq]: (state) =>
    {
        state.status = 'loading';
    },

    [GroupsReqSucc]: (state, payload) =>
    {
        state.status = 'success';
        state.groups = payload;
    },

    [GroupReq]: (state) =>
    {
        state.status = 'loading';
    },

    [GroupReqSucc]: (state, payload) =>
    {
        state.status = 'success';
        //state.events = payload;
    },

    [EditGroupReq]: (state) =>
    {
        state.status = 'loading';
    },

    [EditGroupReqSucc]: (state, payload) =>
    {
        state.status = 'success';
        //state.events = payload;
    },
    
    [AddGroupReq]: (state) =>
    {
        state.status = 'loading';
    },

    [AddGroupReqSucc]: (state, payload) =>
    {
        state.status = 'success';
        //state.events = payload;
    },     

    [GroupErr]: (state, err) =>
    {
        state.status = 'error';
        state.errors = err;
    }
}

export default {
    state,
    getters,
    actions,
    mutations,
}