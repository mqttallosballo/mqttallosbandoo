import
{
    HistoryReq, HistoryReqSucc,
    HistoryErr

} from '@/store/actions.js';

import api from '@/modules/api/index';

const state = {
    history: [],
    errors : "",
    status: ""
}

const getters = {
    getHistory: state => state.history,
} 

const actions =
{
    [HistoryReq]: ({ commit, dispatch }, data) => 
    {
        return new Promise((resolve, reject) =>
        {
            commit(HistoryReq);
            api.myAxiosInstance
                .get('/api/v1/device/'+data.macAddress+'/history?datefrom='+data.datefrom+'T00:00:00&dateto='+data.dateto+'T23:59:59', 
                    { headers: { 'Authorization': 'Token '+localStorage.getItem('user-token') || '', }})
                .then(resp =>
                {
                    commit(HistoryReqSucc, resp.data);
                    resolve(resp);
                })
                .catch(err =>
                {
                    commit(HistoryErr, err);
                    reject(err);
                });
        });
    },  
}
 
const mutations =
{
    [HistoryReq]: (state) =>
    {
        state.status = 'loading';
    },

    [HistoryReqSucc]: (state, payload) =>
    {
        state.status = 'success';
        state.history = payload;
    },

    [HistoryErr]: (state, err) =>
    {
        state.status = 'error';
        state.errors = err;
    }
}

export default {
    state,
    getters,
    actions,
    mutations,
}
