import
{
    JoinReq, JoinReqSucc,
    AddJoinReq, AddJoinReqSucc,
    DelJoinReq, DelJoinReqSucc,
    JoinErr

} from '@/store/actions.js';

import api from '@/modules/api/index';

const state = {
    joins: [],
    errors : "",
    status: "" 
}

const getters = {
    getJoins: state => state.joins,
}

const actions =
{
    [JoinReq]: ({ commit, dispatch }, macAddress) => 
    {
        return new Promise((resolve, reject) =>
        {
            commit(JoinReq);
            api.myAxiosInstance
                .get('/api/v1/device/'+macAddress+'/join', 
                    { headers: { 'Authorization': 'Token '+localStorage.getItem('user-token') || '', }})
                .then(resp =>
                {
                    commit(JoinReqSucc, resp.data);
                    resolve(resp);
                })
                .catch(err =>
                {
                    commit(JoinErr, err.response);
                    reject(err); 
                });
        });
    },
    [AddJoinReq]: ({ commit, dispatch }, data) => 
    {
        return new Promise((resolve, reject) =>
        {
            commit(AddJoinReq);
            let payload = {
                "username" : data.username,
                "role" : data.role
            }

            api.myAxiosInstance
                .post('/api/v1/device/'+data.macAddress+'/join', 
                    payload, 
                    { headers: { 'Authorization': 'Token '+localStorage.getItem('user-token') || '', }})
                .then(resp =>
                {
                    commit(AddJoinReqSucc, resp.data);
                    resolve(resp);
                })
                .catch(err =>
                {
                    commit(JoinErr, err.response);
                    console.log(err)
                    reject(err);
                });
        });
    },
    [DelJoinReq]: ({ commit, dispatch }, data) => 
    {
        return new Promise((resolve, reject) =>
        {
            commit(DelJoinReq);

            api.myAxiosInstance
                .delete('/api/v1/device/'+data.macAddress+'/join/'+data.id,
                    { headers: { 'Authorization': 'Token '+localStorage.getItem('user-token') || '', }})
                .then(resp =>
                {
                    commit(DelJoinReqSucc, resp.data);
                    resolve(resp);
                })
                .catch(err =>
                {
                    commit(JoinErr, err.response);
                    reject(err);
                });
        });
    }       
}

const mutations =
{
    [JoinReq]: (state) =>
    {
        state.status = 'loading';
    },

    [JoinReqSucc]: (state, payload) =>
    {
        state.status = 'success';
        state.joins = payload;
    },

    [AddJoinReq]: (state) =>
    {
        state.status = 'loading';
    },

    [AddJoinReqSucc]: (state, payload) =>
    {
        state.status = 'success';
    },
    [DelJoinReq]: (state) =>
    {
        state.status = 'loading';
    },

    [DelJoinReqSucc]: (state, payload) =>
    {
        state.status = 'success';
    },    

    [JoinErr]: (state, err) =>
    {
        state.status = 'error';
        state.errors = err;
    }
}

export default {
    state,
    getters,
    actions,
    mutations,
}
