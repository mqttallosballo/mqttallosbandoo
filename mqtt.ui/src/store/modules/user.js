import
{
    UserReq, UserReqSucc,
    UserErr, AuthSignOff

} from '@/store/actions.js';

import Vue from 'vue';
import api from '@/modules/api/index';

const state = {
    status: '',
    profile: {},

    errors: null
}

const getters = {
    profile: state => state.profile,
    isProfileLoaded: state => !!state.profile.username
}

const actions =
{
    [UserReq]: ({ commit }) =>
    {
        api.myAxiosInstance
            .get('/user/', 
                { headers: { 'Authorization': 'Token '+localStorage.getItem('user-token') || '', } })
            .then(resp =>
            {
                commit(UserReqSucc, resp.data);
            })
            .catch(err =>
            {
                commit(UserErr, err.response);
                dispatch(AuthSignOff);
            })
    },
}

const mutations =
{
    [UserReq]: (state) =>
    {
        state.status = 'loading';
        Vue.set(state, 'errors', null);
    },

    [UserReqSucc]: (state, payload) =>
    {
        state.status = 'success';
        state.profile = payload;
        Vue.set(state, 'profile', payload);
    },

    [UserErr]: (state, payload) =>
    {
        state.status = 'error';
        Vue.set(state, 'errors', payload);
    },

    [AuthSignOff]: (state) =>
    {
        state.profile = {};
    }
}

export default {
    state,
    getters,
    actions,
    mutations,
}
